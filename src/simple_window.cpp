#include "simple_window.h"

#include <string>

namespace xmedia {

namespace core {
namespace window {

SimpleWindow::SimpleWindow()
    : _handle(nullptr)
    , _width(-1)
    , _height(-1)
{

}

SimpleWindow::~SimpleWindow()
{

}

void SimpleWindow::CreateEmptyWindow()
{
    _handle = createWindow(-1, -1, -1, -1);
}

void SimpleWindow::CreateDefaultWindow()
{
    registerClass(DefWindowProc);
    _handle = createWindow(-1, -1, -1, -1);
}

void SimpleWindow::registerClass(WindowProcedureFunc func)
{
    const std::wstring myclass = L"simple window";

    WNDCLASSEX wndclass = {
        sizeof(WNDCLASSEX),
        CS_DBLCLKS,
        func,
        0, 0,
        GetModuleHandle(0),
        LoadIcon(0,IDI_APPLICATION),
        LoadCursor(0,IDC_ARROW),
        HBRUSH(COLOR_WINDOW + 1),
        0,
        myclass.c_str(),
        LoadIcon(0,IDI_APPLICATION) };

    RegisterClassEx(&wndclass);
}

WindowHandle SimpleWindow::createWindow(int x, int y, int w, int h)
{
#ifdef _WIN32
    return CreateWindowEx(0, 0, L"simple window",
                          WS_OVERLAPPEDWINDOW,
                          x == -1 ? CW_USEDEFAULT : x,
                          y == -1 ? CW_USEDEFAULT : y,
                          w == -1 ? CW_USEDEFAULT : w,
                          h == -1 ? CW_USEDEFAULT : h,
                          nullptr,
                          nullptr,
                          nullptr,
                          nullptr);
#else
    // TODO Something, create sdl window.
#endif
}

void SimpleWindow::MessageLoop()
{
    if (_handle) {
        ShowWindow(_handle, SW_SHOWDEFAULT);
        MSG msg;
        while (true) {
            while (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE)) {
                DispatchMessage(&msg);
            }
        }
    }
}

}   // window
}   // core

}   // xmedia


//LRESULT WINAPI BaseWindow::WindowProcedure(HWND window, unsigned int msg, WPARAM wp, LPARAM lp)
//{
//    switch (msg) {
//    case WM_DESTROY:
//#ifdef USE_CONSOLE_DEBUG
//        std::cout << "\ndestroying window\n";
//#endif
//        PostQuitMessage(0);
//        return 0L;
//    case WM_LBUTTONDOWN:
//#ifdef USE_CONSOLE_DEBUG
//        std::cout << "\nmouse left button down at (" << LOWORD(lp)
//            << ',' << HIWORD(lp) << ")\n";
//#endif
//        break;
//    case WM_RBUTTONDOWN:
//#ifdef USE_CONSOLE_DEBUG
//        std::cout << "\nmouse right button down at (" << LOWORD(lp)
//            << ',' << HIWORD(lp) << ")\n";
//#endif
//        break;
//    default:
//        return DefWindowProc(window, msg, wp, lp);
//    }
//}

