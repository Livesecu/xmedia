#pragma once

#include <condition_variable>
#include <mutex>
#include <list>

#include "decoder/core_commons.h"

namespace xmedia {
namespace core {

class FrameQueue {
 public:
  FrameQueue(int size);
  ~FrameQueue();

  void Put(RawFrame* frame);
  RawFrame* Get();

 private:
  bool isFull();
  bool isEmpty();

  RawFrame** _frames;

  int _readPos;
  int _writePos;
  int _size;

  std::mutex _mtx;
  std::condition_variable _cv;
};

}  // namespace core
}  // namespace xmedia