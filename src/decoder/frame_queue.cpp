#include "frame_queue.h"
#include "log/logger.h"

namespace xmedia {
namespace core {

FrameQueue::FrameQueue(int size)
    : _frames(new RawFrame*[size]), _size(size), _readPos(-1), _writePos(-1) {
  for (int i = 0; i < _size; ++i) {
    _frames[i] = nullptr;
  }
}

FrameQueue::~FrameQueue() {
  // Flush remained frames.
  for (int i = 0; i < _size; ++i) {
    if (_frames[i]) {
      delete _frames[i];
      _frames[i] = nullptr;
    }
  }
  if (_frames) {
    delete[] _frames;
    _frames = nullptr;
  }
  xmedia::log::WriteLog("xmedia", xmedia::log::Level::Debug,
                        "frame queue is destroyed.[r: %d / w: %d]", _readPos,
                        _writePos);
}

void FrameQueue::Put(RawFrame* frame) {
  std::lock_guard<std::mutex> lck(_mtx);

  if (isFull()) {
    delete frame;
    return;
  }
  _writePos = (_writePos + 1) % _size;
  _frames[_writePos] = frame;
  _cv.notify_all();
}

RawFrame* FrameQueue::Get() {
  /*std::unique_lock<std::mutex> lck(_mtx);
  _cv.wait(lck, [&] { return !isEmpty(); });*/

  std::lock_guard<std::mutex> lck(_mtx);

  if (isEmpty()) {
      return nullptr;
  }
  _readPos = (_readPos + 1) % _size;
  auto frame = _frames[_readPos];
  _frames[_readPos] = nullptr;

  return frame;
}

bool FrameQueue::isFull() {
  return ((_writePos + 1) % _size) == _readPos ? true : false;
}

bool FrameQueue::isEmpty() {
  bool ret = false;
  if (_writePos == -1 || _readPos == _writePos) {
    ret = true;
  } else {
    ret = false;
  }
  return ret;
}

}  // namespace core
}  // namespace xmedia