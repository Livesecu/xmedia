#pragma once

#include "core_commons.h"

using GotFrameCallback = void(*)(uint8_t*, int);

namespace livemediacore {

class Demuxer
{
private:
    AVFormatContext* _fmtContext;
    AVStream* _videoStream;
    /*AVStream* _audioStream;*/

    int _videoFrameCount;
    /*int _audioFrameCount;*/

    std::string _filePath;

    std::string _videoCodec;
    int _width;
    int _height;

public:
    //Demuxer();
    Demuxer(const char* filePath);
    virtual ~Demuxer();

    int ReadOneFrame(GotFrameCallback callback);

    std::string VideoCodecString() { return _videoCodec; }
    int Width() { return _width; }
    int Height() { return _height; }

private:
    bool GetStreamInfo();

};

}   // livemediacore