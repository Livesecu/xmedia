#pragma once

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <synchapi.h>

using WindowHandle = HWND;

#else

#endif

// FFMPEG Header Files
#ifdef __cplusplus
extern "C" {
#endif
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libavutil/avutil.h"
#include "libavutil/error.h"
#include "libavutil/imgutils.h"
#include "libswscale/swscale.h"
#ifdef __cplusplus
}
#endif

// LIVE555 Header files
#include "liveMedia.hh"
#include "BasicUsageEnvironment.hh"
#include "GroupsockHelper.hh"

#include <string>
#include <mutex>
#include <condition_variable>

// RTSP
#define RTSP_CLIENT_VERBOSITY_LEVEL 1
#define RECV_BUFFER_SIZE 1000000

//#define USE_CONSOLE_DEBUG

enum class MediaType { None, Video, Audio };

enum class PixelFormat {
  None,
  YV12,
  NV12,
  R8G8B8,
};

enum class PacketType {
  None,
  Timeout,
  Initial,
  Terminate,
};

struct RawFrame {
  MediaType mediaType;

  PixelFormat pixFmt;

  int width, height;

  int64_t pts;
  int64_t dts;

  AVFrame* avFrame;

  RawFrame()
      : avFrame(av_frame_alloc()),
        mediaType(MediaType::None),
        pixFmt(PixelFormat::None),
        width(-1),
        height(-1),
        pts(-1),
        dts(-1) {}

  ~RawFrame() {
    if (avFrame) {
      av_frame_free(&avFrame);
    }
  }
};

struct Packet {
  int index;

  int abortFlag;

  PacketType packetType;

  MediaType mediaType;

  PixelFormat pixFmt;

  uint8_t* data;
  int size;

  char codecName[64];

  int width, height;

  int64_t pts;
  int64_t dts;

  int64_t bitrate;

  AVPacket* avPacket;

  Packet(int size)
      : avPacket(av_packet_alloc()),
        mediaType(MediaType::None),
        index(0),
        abortFlag(0),
        packetType(PacketType::None),
        data(nullptr),
        size(size),
        width(-1),
        height(-1),
        pts(-1),
        dts(-1),
        bitrate(0) {
    data = new uint8_t[size];
    av_init_packet(avPacket);
    avPacket->data = NULL;
    avPacket->size = 0;
  }

  ~Packet() {
    if (avPacket) {
      av_packet_free(&avPacket);
    }

    if (data) {
      delete[] data;
      data = nullptr;
    }
  }
};