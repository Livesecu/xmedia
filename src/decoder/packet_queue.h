#pragma once

#include <condition_variable>
#include <list>
#include <mutex>

#include "decoder/core_commons.h"

namespace xmedia {
namespace core {

class PacketQueue {
 public:
  PacketQueue(int size);
  ~PacketQueue();

  void Put(Packet* pkt);
  Packet* Get();

 private:
  bool isFull();
  bool isEmpty();

  Packet** _packets;
  // std::list<Packet*> _packets;

  int _readPos;
  int _writePos;
  int _size;

  std::mutex _mtx;
  std::condition_variable _cv;
};

}  // namespace core
}  // namespace xmedia