#include "demuxer.h"

namespace livemediacore {

//Demuxer::Demuxer()
//    : _fmtContext(avformat_alloc_context())
//    , _videoStream(nullptr)
//    , _audioStream(nullptr)
//    , _videoFrameCount(-1)
//    , _audioFrameCount(-1)
//{
//    
//}

Demuxer::Demuxer(const char* filePath)
    : _fmtContext(nullptr)
    , _videoStream(nullptr)
    /*, _audioStream(nullptr)*/
    , _videoFrameCount(-1)
    /*, _audioFrameCount(-1)*/
    , _filePath(filePath)
{
    if (avformat_open_input(&_fmtContext, filePath, NULL, NULL) < 0) {
        fprintf(stderr, "avformat open input error\n");
    }

    if (!GetStreamInfo()) {
        fprintf(stderr, "failed to get stream information\n");
    }
}


Demuxer::~Demuxer()
{
    if (_fmtContext) {
        if (!_filePath.empty()) {
            avformat_close_input(&_fmtContext);
        }
        else {
            avformat_free_context(_fmtContext);
        }
    }
}

int Demuxer::ReadOneFrame(GotFrameCallback callback)
{
    AVPacket* packet = av_packet_alloc();   // re-use it
    int ret = av_read_frame(_fmtContext, packet);
    if (ret < 0) {
        av_packet_free(&packet);
        return ret;
    }

    callback(packet->data, packet->size);

    av_packet_free(&packet);
    return ret;
}

bool Demuxer::GetStreamInfo()
{
    auto ret = avformat_find_stream_info(_fmtContext, NULL);
    if (ret < 0) {
        return false;
    }

    // NOTE : Now we get only video streams
    auto vidStreamIndex = av_find_best_stream(_fmtContext, AVMediaType::AVMEDIA_TYPE_VIDEO, -1, -1, NULL, 0);
    _videoStream = _fmtContext->streams[vidStreamIndex];
    if (_videoStream == nullptr) {
        return false;
    }

    if (_videoStream->codecpar->codec_id == AV_CODEC_ID_H264) {
        _videoCodec = "H264";
    }

    _width = _videoStream->codecpar->width;
    _height = _videoStream->codecpar->height;

    return true;
}

}   // livemediacore