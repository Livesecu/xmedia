#pragma once

#include "decoder/core_commons.h"

// using OnDecodeCallback = void(*)(uint8_t**, int*);

namespace xmedia {
namespace core {

class Decoder {
 public:
  Decoder();
  virtual ~Decoder();

  bool Initialize(const char* codecName);

  int DecodeOnePacket(uint8_t* data, unsigned size, uint8_t* dstData[],
                      int dstSize[]);
  // void DecodeOnePacket(uint8_t* data, unsigned size, OnDecodeCallback
  // callback);
  int DecodeOnePacket(Packet* packet, RawFrame* frame);

 private:
  AVCodecContext* _codecContext;
  AVCodecParserContext* _codecParser;
  AVCodec* _codec;
};

static AVCodecID SearchCodecByName(const char* codecName) {
  if (!strcmp(codecName, "H264"))
    return AV_CODEC_ID_H264;
  else
    return AV_CODEC_ID_NONE;
}

static AVCodec* FindCodec(const char* codecName) {
  auto codec = avcodec_find_decoder_by_name(codecName);
  if (!codec) {
    codec = avcodec_find_decoder(SearchCodecByName(codecName));
    if (!codec) {
      return nullptr;
    }
  }
  return codec;
}

}  // namespace core
}  // namespace xmedia