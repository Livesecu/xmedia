#include "decoder.h"
#include "log/logger.h"

namespace xmedia {
namespace core {

Decoder::Decoder()
    : _codecContext(nullptr), _codecParser(nullptr), _codec(nullptr) {
  xmedia::log::WriteLog("xmedia", xmedia::log::Level::Debug,
                        "new decoder is created.");
}

Decoder::~Decoder() {
  av_parser_close(_codecParser);
  avcodec_free_context(&_codecContext);
  xmedia::log::WriteLog("xmedia", xmedia::log::Level::Debug,
                        "decoder is destroyed.");
}

bool Decoder::Initialize(const char* codecName) {
  ////log::api::WriteLog(CORE_LOGGER, log::Level::Info, "Try to initialize a
  ///decoder...");

  auto codec = FindCodec(codecName);
  if (!codec) {
    ////log::api::WriteLog(CORE_LOGGER, log::Level::Err, "Failed to find a
    ///decoder"); /log::api::WriteLog(CORE_LOGGER, log::Level::Err, "\t codec
    ///name: [%s]", codecName);
    return false;
  }

  _codecContext = avcodec_alloc_context3(codec);
  if (!_codecContext) {
    ////log::api::WriteLog(CORE_LOGGER, log::Level::Err, "Failed to create a
    ///codec context");
    return false;
  }

  _codecParser = av_parser_init(codec->id);
  if (!_codecParser) {
    ////log::api::WriteLog(CORE_LOGGER, log::Level::Err, "Failed to create a
    ///codec parser");
    return false;
  }

  if (avcodec_open2(_codecContext, codec, NULL) < 0) {
    ////log::api::WriteLog(CORE_LOGGER, log::Level::Err, "Failed to open the
    ///decoder");
    return false;
  }

  ////log::api::WriteLog(CORE_LOGGER, log::Level::Info, "Success to initialize a
  ///decoder...");

  return true;
}

int Decoder::DecodeOnePacket(Packet* packet, RawFrame* frame) {
  auto data = packet->data;
  auto size = packet->size;

  auto avPacket = packet->avPacket;
  auto avFrame = frame->avFrame;

  while (data && size) {
    int len = av_parser_parse2(_codecParser, _codecContext, &avPacket->data,
                               &avPacket->size, data, size, packet->pts,
                               AV_NOPTS_VALUE, 0);
    data += len;
    size -= len;

    if (avPacket->size) {
      int ret = 0;

      ret = avcodec_send_packet(_codecContext, avPacket);
      if (ret < 0) {
        ////log::api::WriteLog(CORE_LOGGER, log::Level::Warn, "Error sending a
        ///packet for decoding");
        return ret;
      }

      while (ret >= 0) {
        ret = avcodec_receive_frame(_codecContext, avFrame);
        if (ret < 0) {
          // those two return values are special and mean there is no output
          // frame available, but there were no errors during decoding
          if (ret == AVERROR_EOF || ret == AVERROR(EAGAIN)) return 0;

          char err[1024];
          av_strerror(ret, err, sizeof err);
          ////log::api::WriteLog(CORE_LOGGER, log::Level::Warn, "Error during
          ///decoding: %s", err);

          return ret;
        }

        if (!ret) {
          frame->width = avFrame->width;
          frame->height = avFrame->height;
          frame->pts = avFrame->pts;

          break;
        }
      }
    }
  }

  return 0;
}

// void Decoder::DecodeOnePacket(uint8_t* data, unsigned size, OnDecodeCallback
// callback)
//{
//    AVPacket* packet = av_packet_alloc();
//    AVFrame* frame = av_frame_alloc();
//
//    av_init_packet(packet);
//
//    while (data && size) {
//        int len = av_parser_parse2(_codecParser, _codecContext,
//                                   &packet->data, &packet->size,
//                                   data, size,
//                                   AV_NOPTS_VALUE, AV_NOPTS_VALUE, 0);
//        data += len;
//        size -= len;
//
//        if (packet->size) {
//            int ret = 0;
//
//            ret = avcodec_send_packet(_codecContext, packet);
//            if (ret < 0) {
//                fprintf(stderr, "Error sending a packet for decoding\n");
//                goto clean;
//            }
//
//
//            while ((ret = avcodec_receive_frame(_codecContext, frame)) >= 0) {
//                if (ret == AVERROR(EAGAIN)) {
//                    fprintf(stderr, "ERROR AGAIN\n");
//                    goto clean;
//                }
//                else if (ret == AVERROR_EOF) {
//                    fprintf(stderr, "EOF\n");
//                    goto clean;
//                }
//                else if (ret == AVERROR_INPUT_CHANGED) {
//                    fprintf(stderr, "AVERROR_INPUT_CHANGED\n");
//                }
//                else if (ret < 0) {
//                    char err[1024];
//                    av_strerror(ret, err, sizeof err);
//                    fprintf(stderr, "Error during decoding: %s\n", err);
//                    goto clean;
//                }
//            }
//
//            callback(frame->data, frame->linesize);
//        }
//    }
//
//    av_packet_free(&packet);
//    av_frame_free(&frame);
//
// clean:
//    av_packet_free(&packet);
//    av_frame_free(&frame);
//}

}  // namespace core
}  // namespace xmedia