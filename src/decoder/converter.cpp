#include "converter.h"

namespace xmedia {
namespace core {
namespace utils {

ImageConverter::ImageConverter()
    : _convertContext(nullptr)

{

}

ImageConverter::~ImageConverter()
{
    if (_convertContext) {
        sws_freeContext(_convertContext);
        _convertContext = nullptr;
    }
}

//int ImageConverter::Rescale(/*source*/uint8_t* srcData[], int srcSize[], int srcWidth, int srcHeight, PixelFormat srcFormat,
//                            /*dest*/uint8_t* dstData[4], int dstSize[4], int dstWidth, int dstHeight, PixelFormat dstFormat)
//{
//    _convertContext = sws_getCachedContext(_convertContext,
//                                           srcWidth, srcHeight, AV_PIX_FMT_YUV420P,
//                                           dstWidth, dstHeight, AV_PIX_FMT_YUV420P,
//                                           SWS_BICUBIC, NULL, NULL, NULL);
//
//    if (!_convertContext) {
//        fprintf(stderr, "No SwsContext\n");
//        return -1;
//    }
//
//    /*if (_data && _size) {
//        av_freep(&_data[0]);
//        memset(_size, 0, sizeof _size);
//    }*/
//
//    int requiredSize = av_image_alloc(resizedData, resizedDataSize,
//                                      dstWidth, dstHeight,
//                                      AV_PIX_FMT_YUV420P,
//                                      1);
//
//    return sws_scale(_convertContext, srcData, srcSize, 0, srcHeight, dstData, dstSize);
//}

int ImageConverter::Rescale(/*source*/RawFrame* frame,
                            /*dest*/uint8_t* dstData[4], int dstSize[4], int dstWidth, int dstHeight, PixelFormat dstFormat)
{
    _convertContext = sws_getCachedContext(_convertContext,
                                           frame->width, frame->height, AV_PIX_FMT_YUV420P,
                                           dstWidth, dstHeight, AV_PIX_FMT_YUV420P,
                                           SWS_BICUBIC, NULL, NULL, NULL);

    if (!_convertContext) {
        fprintf(stderr, "No SwsContext\n");
        return -1;
    }

    /*if (_data && _size) {
        av_freep(&_data[0]);
        memset(_size, 0, sizeof _size);
    }*/

    int requiredSize = av_image_alloc(dstData, dstSize,
                                      dstWidth, dstHeight,
                                      AV_PIX_FMT_YUV420P,
                                      1);

    int ret = sws_scale(_convertContext,
                        (const uint8_t* const*)frame->avFrame->data, frame->avFrame->linesize, 0, frame->height, dstData, dstSize);

    if (ret <= 0)
        return ret;

    return requiredSize;
}

}   // utils
}   // core
}   // livemediacore