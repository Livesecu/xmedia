#include "packet_queue.h"

#include <chrono>

#include "log/logger.h"

namespace xmedia {
namespace core {

PacketQueue::PacketQueue(int size)
    : _packets(new Packet*[size]), _size(size), _readPos(-1), _writePos(-1) {
  for (int i = 0; i < _size; ++i) {
    _packets[i] = nullptr;
  }
}

PacketQueue::~PacketQueue() {
  for (int i = 0; i < _size; ++i) {
    if (_packets[i]) {
      delete _packets[i];
      _packets[i] = nullptr;
    }
  }
  if (_packets) {
    delete[] _packets;
    _packets = nullptr;
  }
  xmedia::log::WriteLog("xmedia", xmedia::log::Level::Debug,
                        "packet queue is destroyed.[r: %d / w: %d]", _readPos,
                        _writePos);
}

void PacketQueue::Put(Packet* packet) {
  std::lock_guard<std::mutex> lck(_mtx);

  if (isFull()) {
    fprintf(stderr, "packet queue is full\n");
    delete packet;
    packet = nullptr;
    return;
  }
  _writePos = (_writePos + 1) % _size;
  _packets[_writePos] = packet;
  _cv.notify_all();
}

Packet* PacketQueue::Get() {
  std::unique_lock<std::mutex> lck(_mtx);
  _cv.wait(lck, [&] { return !isEmpty(); });

  _readPos = (_readPos + 1) % _size;
  auto pkt = _packets[_readPos];
  _packets[_readPos] = nullptr;

  return pkt;
}

bool PacketQueue::isFull() {
  return ((_writePos + 1) % _size) == _readPos ? true : false;
}

bool PacketQueue::isEmpty() {
  bool ret = false;
  if (_writePos == -1 || _readPos == _writePos) {
    ret = true;
  } else {
    ret = false;
  }

  return ret;
}

}  // namespace core
}  // namespace xmedia