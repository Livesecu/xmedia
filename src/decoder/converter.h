#pragma once

//#include "xmedia_core.h"
#include "core_commons.h"

/// <summary>
/// source: data buffer, size, width, height, pixel format
/// target: width, height, pixelformat, empty data buffer, empty size
/// </summary>
namespace xmedia {
namespace core {
namespace utils {

class ImageConverter {
 public:
  ImageConverter();
  virtual ~ImageConverter();

  // int Rescale(/*source*/uint8_t* srcData[], int srcSize[], int srcWidth, int
  // srcHeight, PixelFormat srcFormat,
  //            /*dest*/uint8_t* dstData[4], int dstSize[4], int dstWidth, int
  //            dstHeight, PixelFormat dstFormat);
  int Rescale(/*source*/ RawFrame* frame,
              /*dest*/ uint8_t* dstData[4], int dstSize[4], int dstWidth,
              int dstHeight, PixelFormat dstFormat);

  SwsContext* Get() { return _convertContext; }

 private:
  SwsContext* _convertContext;

  uint8_t* _data[4];
  int _size[4];
};

}  // namespace utils
}  // namespace core
}  // namespace xmedia