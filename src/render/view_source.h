#pragma once

#include <string>
#include <thread>

#include "decoder/core_commons.h"
#include "decoder/decoder.h"
#include "decoder/frame_queue.h"
#include "player_commons.h"
#include "stream/network_stream.h"

namespace xmedia {
namespace player {

class ViewSource {
 public:
  ViewSource(const SourceConnection& connection);
  ~ViewSource();

  RawFrame* GetFrame();

 private:
  void ReadAndDecode(xmedia::core::Decoder* decoder);

  SourceConnection connection_;

  xmedia::core::NetworkStreamReader* stream_{nullptr};
  xmedia::core::FrameQueue* queue_{nullptr};
  std::thread* thread_{nullptr};
};

}  // namespace player
}  // namespace xmedia