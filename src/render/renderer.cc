#include "renderer.h"

#include "util/util.h"

Renderer::Renderer()
    : d3d_device_(nullptr), d3d_device_ex_(nullptr), d3d_rts_(nullptr) {}

Renderer::~Renderer() {
  SAFE_RELEASE(d3d_device_);
  SAFE_RELEASE(d3d_device_ex_);
  SAFE_RELEASE(d3d_rts_);
}

HRESULT Renderer::CheckDeviceState() {
  if (d3d_device_ex_) {
    // https://docs.microsoft.com/en-us/windows/win32/api/d3d9/nf-d3d9-idirect3ddevice9ex-checkdevicestate
    return d3d_device_ex_->CheckDeviceState(NULL);
  } else if (d3d_device_) {
    // https://docs.microsoft.com/en-us/windows/win32/api/d3d9helper/nf-d3d9helper-idirect3ddevice9-testcooperativelevel
    return d3d_device_->TestCooperativeLevel();
  } else {
    return D3DERR_DEVICELOST;
  }
}

HRESULT Renderer::CreateSurface(UINT width, UINT height, bool use_alpha,
                                UINT num_samples) {
  HRESULT hr = S_OK;
  SAFE_RELEASE(d3d_rts_);

  IFC(d3d_device_->CreateRenderTarget(
      width, height, use_alpha ? D3DFMT_A8R8G8B8 : D3DFMT_X8R8G8B8,
      static_cast<D3DMULTISAMPLE_TYPE>(num_samples), 0,
      d3d_device_ex_ ? FALSE : TRUE, &d3d_rts_, NULL));

  IFC(d3d_device_->SetRenderTarget(0, d3d_rts_));

Cleanup:
  return hr;
}

HRESULT Renderer::Init(IDirect3D9* d3d, IDirect3D9Ex* d3d_ex, HWND hwnd,
                       UINT adapter) {
  HRESULT hr = S_OK;

  D3DPRESENT_PARAMETERS d3dpp;
  SecureZeroMemory(&d3dpp, sizeof(d3dpp));
  d3dpp.Windowed = TRUE;
  d3dpp.BackBufferFormat = D3DFMT_UNKNOWN;
  d3dpp.BackBufferHeight = 1;
  d3dpp.BackBufferWidth = 1;
  d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;

  D3DCAPS9 caps;
  DWORD vertex_processing;
  IFC(d3d->GetDeviceCaps(adapter, D3DDEVTYPE_HAL, &caps));
  if ((caps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT) ==
      D3DDEVCAPS_HWTRANSFORMANDLIGHT) {
    vertex_processing = D3DCREATE_HARDWARE_VERTEXPROCESSING;
  } else {
    vertex_processing = D3DCREATE_SOFTWARE_VERTEXPROCESSING;
  }

  if (d3d_ex) {
    IDirect3DDevice9Ex* d3d_device = NULL;
    IFC(d3d_ex->CreateDeviceEx(
        adapter, D3DDEVTYPE_HAL, hwnd,
        vertex_processing | D3DCREATE_MULTITHREADED | D3DCREATE_FPU_PRESERVE,
        &d3dpp, NULL, &d3d_device_ex_));
    IFC(d3d_device_ex_->QueryInterface(__uuidof(IDirect3DDevice9),
                                       reinterpret_cast<void**>(&d3d_device_)));
  } else {
    assert(d3d);
    IFC(d3d->CreateDevice(
        adapter, D3DDEVTYPE_HAL, hwnd,
        vertex_processing | D3DCREATE_MULTITHREADED | D3DCREATE_FPU_PRESERVE,
        &d3dpp, &d3d_device_));
  }

Cleanup:
  return hr;
}