#pragma once

#include "renderer.h"

class VideoRenderer : public Renderer {
 public:
  static HRESULT Create(IDirect3D9* d3d, IDirect3D9Ex* d3d_ex, HWND hwnd,
                        UINT adapter, Renderer** renderer);
  ~VideoRenderer() override;

  HRESULT CreateSurface(UINT width, UINT height, bool use_alpha,
                        UINT num_samples) override;
  HRESULT Render() override;

 protected:
  HRESULT Init(IDirect3D9* d3d, IDirect3D9Ex* d3d_ex, HWND hwnd,
               UINT adapter) override;

 private:
  VideoRenderer();

  IDirect3DSurface9* offsurface_{nullptr};
};