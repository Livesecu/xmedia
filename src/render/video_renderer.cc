#include "video_renderer.h"

#include "util/util.h"

#define D3DFMT_YV12 (D3DFORMAT) MAKEFOURCC('Y', 'V', '1', '2')
#define D3DFMT_NV12 (D3DFORMAT) MAKEFOURCC('N', 'V', '1', '2')

VideoRenderer::VideoRenderer() : Renderer() {}

VideoRenderer::~VideoRenderer() { SAFE_RELEASE(offsurface_); }

HRESULT VideoRenderer::Create(IDirect3D9* d3d, IDirect3D9Ex* d3d_ex, HWND hwnd,
                              UINT adapter, Renderer** renderer) {
  HRESULT hr = S_OK;
  VideoRenderer* video_renderer = new VideoRenderer();

  IFCOOM(video_renderer);
  IFC(video_renderer->Init(d3d, d3d_ex, hwnd, adapter));
  *renderer = video_renderer;
  video_renderer = nullptr;

Cleanup:
  delete video_renderer;
  return hr;
}

HRESULT VideoRenderer::CreateSurface(UINT width, UINT height, bool use_alpha,
                                     UINT num_samples) {
  HRESULT hr = S_OK;
  Renderer::CreateSurface(width, height, use_alpha, num_samples);
  d3d_device_->CreateOffscreenPlainSurface(width, height, D3DFMT_YV12,
                                           D3DPOOL_DEFAULT, &offsurface_, NULL);
  return hr;
}

HRESULT VideoRenderer::Render() {
  HRESULT hr = S_OK;

return hr;

}

HRESULT VideoRenderer::Init(IDirect3D9* d3d, IDirect3D9Ex* d3d_ex, HWND hwnd,
                            UINT adapter) {
  HRESULT hr = S_OK;
  IFC(Renderer::Init(d3d, d3d_ex, hwnd, adapter));

Cleanup:
  return hr;
}