#pragma once

#include <memory>
#include <string>

struct VideoConnection {
  std::string url;
  std::string user;
  std::string password;
  bool use_tcp;
};

class VideoSource {
 public:
  explicit VideoSource(std::unique_ptr<VideoConnection> connect);
  ~VideoSource();

 private:
  std::unique_ptr<VideoConnection> connect_;
};