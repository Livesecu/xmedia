#pragma once

#include <unordered_map>
#include <memory>

#include "render/view_box.h"
#include "player_commons.h"

namespace xmedia {
namespace player {

class Render {
 public:
  virtual ~Render() = default;
  virtual bool InitGraphics() = 0;
  virtual IDirect3DSurface9* BackBuffer() = 0;
  virtual void Display(
      const std::unordered_map<int, std::unique_ptr<ViewBox>>& viewBoxMap) = 0;
  virtual void Present(RECT* pSourceRect, RECT* pDestRect,
                       HWND hDestWindowOverride, RGNDATA* pDirtyRegion) = 0;
};

}  // namespace player
}  // namespace xmedia