#pragma once

#define WIN32_LEAN_AND_MEAN  // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <assert.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <windows.h>

class Renderer {
 public:
  virtual ~Renderer();

  HRESULT CheckDeviceState();
  virtual HRESULT CreateSurface(UINT width, UINT height, bool use_alpha,
                        UINT num_samples);
  virtual HRESULT Render() = 0;

  IDirect3DSurface9* surface_noref() { return d3d_rts_; }

 protected:
  Renderer();

  virtual HRESULT Init(IDirect3D9* d3d, IDirect3D9Ex* d3d_ex, HWND hwnd,
                       UINT adapter);

  IDirect3DDevice9* d3d_device_;
  IDirect3DDevice9Ex* d3d_device_ex_;
  IDirect3DSurface9* d3d_rts_;
};