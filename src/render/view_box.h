#pragma once

#include "player_commons.h"
#include "view_source.h"

namespace xmedia {
namespace player {

class ViewBox {
 public:
  explicit ViewBox(const ViewPosition& vp);
  ~ViewBox();

  BoxType GetType() { return box_type_; }
  void SetType(BoxType type) { box_type_ = type; }
  ViewPosition GetViewPos() { return view_position_; }

  ViewSource* Source() const { return source_; }

  void AddViewSource(const SourceConnection& connection);
  void RemoveViewSource();

  // Prevent creating the view box without the position and copying the view box
  // itself
  ViewBox() = delete;
  ViewBox(const ViewBox& other) = delete;
  ViewBox& operator=(const ViewBox& other) = delete;

 private:
  ViewPosition view_position_;
  BoxType box_type_;

  ViewSource* source_;
};

}  // namespace player
}  // namespace xmedia


