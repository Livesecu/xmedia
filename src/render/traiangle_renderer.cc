#include "util/util.h"

#include "triangle_renderer.h"

struct CUSTOMVERTEX {
  FLOAT x, y, z;
  DWORD color;
};

#define D3DFVF_CUSTOMVERTEX (D3DFVF_XYZ | D3DFVF_DIFFUSE)

TriangleRenderer::TriangleRenderer() : Renderer(), vb_(nullptr) {

}

TriangleRenderer::~TriangleRenderer() { SAFE_RELEASE(vb_); }

HRESULT TriangleRenderer::Create(IDirect3D9* d3d, IDirect3D9Ex* d3d_ex,
                                 HWND hwnd, UINT adapter,
               Renderer** renderer)
{
  HRESULT hr = S_OK;

  TriangleRenderer* pRenderer = new TriangleRenderer();
  IFCOOM(pRenderer);

  IFC(pRenderer->Init(d3d, d3d_ex, hwnd, adapter));

  *renderer = pRenderer;
  pRenderer = NULL;

Cleanup:
  delete pRenderer;

  return hr;
}

HRESULT TriangleRenderer::Init(IDirect3D9* d3d, IDirect3D9Ex* d3d_ex, HWND hwnd,
                             UINT adapter) {
  HRESULT hr = S_OK;
  D3DXMATRIXA16 matView, matProj;
  D3DXVECTOR3 vEyePt(0.0f, 0.0f, -5.0f);
  D3DXVECTOR3 vLookatPt(0.0f, 0.0f, 0.0f);
  D3DXVECTOR3 vUpVec(0.0f, 1.0f, 0.0f);
  // Set up the VB
  CUSTOMVERTEX vertices[] = {
      {
          -1.0f,
          -1.0f,
          0.0f,
          0xffff0000,
      },  // x, y, z, color
      {
          1.0f,
          -1.0f,
          0.0f,
          0xff00ff00,
      },
      {
          0.0f,
          1.0f,
          0.0f,
          0xff00ffff,
      },
  };

  // Call base to create the device and render target
  IFC(Renderer::Init(d3d, d3d_ex, hwnd, adapter));

  IFC(d3d_device_->CreateVertexBuffer(sizeof(vertices), 0, D3DFVF_CUSTOMVERTEX,
                                       D3DPOOL_DEFAULT, &vb_, NULL));

  void* pVertices;
  IFC(vb_->Lock(0, sizeof(vertices), &pVertices, 0));
  memcpy(pVertices, vertices, sizeof(vertices));
  vb_->Unlock();

  // Set up the camera
  D3DXMatrixLookAtLH(&matView, &vEyePt, &vLookatPt, &vUpVec);
  IFC(d3d_device_->SetTransform(D3DTS_VIEW, &matView));
  D3DXMatrixPerspectiveFovLH(&matProj, D3DX_PI / 4, 1.0f, 1.0f, 100.0f);
  IFC(d3d_device_->SetTransform(D3DTS_PROJECTION, &matProj));

  // Set up the global state
  IFC(d3d_device_->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE));
  IFC(d3d_device_->SetRenderState(D3DRS_LIGHTING, FALSE));
  IFC(d3d_device_->SetStreamSource(0, vb_, 0, sizeof(CUSTOMVERTEX)));
  IFC(d3d_device_->SetFVF(D3DFVF_CUSTOMVERTEX));

Cleanup:
  return hr;
}

HRESULT TriangleRenderer::Render() {
  HRESULT hr = S_OK;
  D3DXMATRIXA16 matWorld;

  // Set up the rotation
  UINT iTime = GetTickCount64() % 1000;
  FLOAT fAngle = iTime * (2.0f * D3DX_PI) / 1000.0f;

  IFC(d3d_device_->BeginScene());
  IFC(d3d_device_->Clear(
      0, NULL, D3DCLEAR_TARGET,
      D3DCOLOR_ARGB(128, 0, 0, 128),  // NOTE: Premultiplied alpha!
      1.0f, 0));

  D3DXMatrixRotationY(&matWorld, fAngle);
  IFC(d3d_device_->SetTransform(D3DTS_WORLD, &matWorld));

  IFC(d3d_device_->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 1));

  IFC(d3d_device_->EndScene());

Cleanup:
  return hr;
}