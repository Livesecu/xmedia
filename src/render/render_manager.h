#pragma once

#include <unordered_map>
#include "renderer.h"
#include "view_container.h"

class RenderManager {
 public:
  static HRESULT Create(RenderManager** manager);
  ~RenderManager();

  //HRESULT EnsureDevices();
  HRESULT Render();

  void set_size(UINT width, UINT height);
  void set_alpha(bool use_alpha);
  void set_num_desired_samples(UINT num_samples);
  void set_adapter(POINT screenspace_point);

  HRESULT backbuffer_noref(IDirect3DSurface9** surface);


  // Control view.
  void AddViewContainer(int container_id, std::unique_ptr<ViewContainer> view_container);
  
 private:
  RenderManager();

  HRESULT EnsureRenderers();
  HRESULT EnsureHwnd();
  HRESULT EnsureD3DObjects();
  HRESULT TestSurfaceSettings();
  void CleanupInvalidDevices();
  void DestroyResources();

  IDirect3D9* d3d_;
  IDirect3D9Ex* d3d_ex_;

  UINT adapters_;
  Renderer** renderers_;
  Renderer* current_renderer_;

  HWND hwnd_;

  UINT width_;
  UINT height_;
  UINT num_samples_;
  bool use_alpha_;
  bool surface_settings_changed_;

  std::unordered_map<int, std::unique_ptr<ViewContainer>> view_container_map_;
};