#include "view_source.h"
#include "log/logger.h"

namespace xmedia {
namespace player {

ViewSource::ViewSource(const SourceConnection& connection)
    : connection_(connection) {
  stream_ = core::NetworkStreamReader::Create(connection_.url);
  if (stream_) {
    if (stream_->OpenStream(connection_.url, connection_.user,
                            connection_.password, connection_.useTCP)) {
      queue_ = new xmedia::core::FrameQueue(10);
      auto decoder = new xmedia::core::Decoder();
      decoder->Initialize("h264");
      thread_ = new std::thread(&ViewSource::ReadAndDecode, this, decoder);
    }
  }
  xmedia::log::WriteLog("xmedia", xmedia::log::Level::Debug,
                        "new view source is created.");
}

ViewSource::~ViewSource() {
  if (stream_) stream_->CloseStream();
  if (thread_) {
    thread_->join();
    delete thread_;
    thread_ = nullptr;
  }
  if (queue_) {
    delete queue_;
    queue_ = nullptr;
  }
  xmedia::log::WriteLog("xmedia", xmedia::log::Level::Debug,
                        "view source is destroyed.");
}

RawFrame* ViewSource::GetFrame() {
  RawFrame* frame{nullptr};

  if (queue_) frame = queue_->Get();
  return frame;
}

void ViewSource::ReadAndDecode(xmedia::core::Decoder* decoder) {
  xmedia::log::WriteLog("xmedia", xmedia::log::Level::Info,
                        "decoder thread start..");
  auto thread_decoder = decoder;

  for (;;) {
    Packet* packet = stream_->ReadStream();

    if (!packet || packet->abortFlag) {
      delete packet;
      packet = nullptr;
      xmedia::log::WriteLog("xmedia", xmedia::log::Level::Info,
                            "abort packet founded.");
      break;
    }

    if (packet->data && packet->size > 0) {
      auto frame = new RawFrame();
      thread_decoder->DecodeOnePacket(packet, frame);
      if (frame->width > 0 && frame->height > 0) {
        queue_->Put(frame);
      } else {
        delete frame;
        frame = nullptr;
      }
      delete packet;
      packet = nullptr;
    }
  }

  delete thread_decoder;
  thread_decoder = nullptr;

  delete stream_;
  stream_ = nullptr;

  xmedia::log::WriteLog("xmedia", xmedia::log::Level::Info,
                        "decoder thread end..");
}

}  // namespace player
}  // namespace xmedia