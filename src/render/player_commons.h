#pragma once

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <d3dx9math.h>

#define D3DFMT_YV12 (D3DFORMAT) MAKEFOURCC('Y', 'V', '1', '2')
#define D3DFMT_NV12 (D3DFORMAT) MAKEFOURCC('N', 'V', '1', '2')

//#define USE_YUV_DEBUG

#include <string>

namespace xmedia {
namespace player {

enum class BoxType {
  Empty,
  Video,
};

struct ViewPosition {
  double x;
  double y;
  double w;
  double h;

  double border;

  ViewPosition() : x(0.0f), y(0.0f), w(0.0f), h(0.0f), border(0.0f) {}

  ViewPosition(double x, double y, double w, double h, double border)
      : x(x), y(y), w(w), h(h), border(border) {}

  // ViewPosition(const ViewPosition& other)
  //    : x(other.x), y(other.y), w(other.w), h(other.h)
  //{
  //}

  // ViewPosition& operator=(const ViewPosition& other)
  //{
  //    if (this == &other) {
  //        return *this;
  //    }

  //    x = other.x;
  //    y = other.y;
  //    w = other.w;
  //    h = other.h;

  //    return *this;
  //}
};

struct SourceConnection {
  std::string url;
  std::string user;
  std::string password;
  bool useTCP;

  SourceConnection() = default;
  SourceConnection(const std::string& url, const std::string& user,
                   const std::string& pasword, bool useTCP)
      : url(url), user(user), password(password), useTCP(useTCP) {}
  SourceConnection(const char* url, const char* user, const char* password,
                   bool useTCP)
      : url(url), user(user), password(password), useTCP(useTCP) {}
};

}  // namespace player

}  // namespace xmedia