#pragma once

#include <memory>
#include <unordered_map>

#include "decoder/converter.h"
#include "render.h"
#include "view_box.h"

namespace xmedia {
namespace player {
namespace dx9 {

class RenderHardware {
 public:
  RenderHardware();

 private:
  unsigned adapter_count_;
  MONITORINFO monitor_info_;
  D3DDISPLAYMODE* display_mode_;
};

class RenderEngine {
 public:
  RenderEngine(RenderHardware* render_hw);

  void Init();
  bool EnsureDevice();

 private:
  IDirect3D9* d3d_;
  IDirect3DDevice9* d3ddev_;
  IDirect3DSurface9* backbuffer_;

  xmedia::core::utils::ImageConverter* resizer_;

  HWND win_handle_;
  int width_;
  int height_;
};

class VideoRender : public Render {
 public:
  ~VideoRender() override;

  void Display(const std::unordered_map<int, std::unique_ptr<ViewBox>>&
                   viewBoxMap) override;
  void Present(RECT* pSourceRect, RECT* pDestRect, HWND hDestWindowOverride,
               RGNDATA* pDirtyRegion) override;

  void FillVideoScreen(const ViewPosition& pos, uint8_t* y, uint8_t* u,
                       uint8_t* v, int ySize, int uvSize);

 private:
  IDirect3DSurface9* offscreen_;
};

class ImageRender : public Render {
 public:
  ~ImageRender() override;

  void Display(const std::unordered_map<int, std::unique_ptr<ViewBox>>&
                   viewBoxMap) override;
  void Present(RECT* pSourceRect, RECT* pDestRect, HWND hDestWindowOverride,
               RGNDATA* pDirtyRegion) override;

  void DrawImage(const ViewPosition& vp);

 private:
  IDirect3DTexture9* d3d_texture_;
  ID3DXSprite* d3d_sprite_;

  std::wstring image_path_;
};

class Dx9Render : public Render {
 public:
  Dx9Render(HWND win);
  ~Dx9Render() override;

  bool InitGraphics() override;
  IDirect3DSurface9* BackBuffer() override { return _backBuffer; }
  void Display(const std::unordered_map<int, std::unique_ptr<ViewBox>>&
                   viewBoxMap) override;
  void Present(RECT* pSourceRect, RECT* pDestRect, HWND hDestWindowOverride,
               RGNDATA* pDirtyRegion) override;

 private:
  void GetDisplayMode();
  bool EnsureDevice();
  bool EnsureFormatConversion();

  void FillImageBuffer(const ViewPosition& pos, uint8_t* y, uint8_t* u,
                       uint8_t* v, int ySize, int uvSize);

  void DrawLogoImage(const ViewPosition& vp);

  IDirect3D9* _d3d;
  IDirect3DDevice9* _dev;
  IDirect3DSurface9* _backBuffer;

  IDirect3DSurface9* _offscreenSurface;

  IDirect3DTexture9* _texture;
  ID3DXSprite* _sprite;

  D3DDISPLAYMODE* _displayModes;

  xmedia::core::utils::ImageConverter* _resizer;

  unsigned _adapterCount;
  MONITORINFO _monitor;

  HWND _win;
  int _width;
  int _height;
};

}  // namespace dx9
}  // namespace player
}  // namespace xmedia