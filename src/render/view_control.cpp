#include "view_control.h"

#include <random>

#include "dx9_render.h"
#include "log/logger.h"

namespace xmedia {
namespace player {

ViewControl::ViewControl(HWND win)
    : win_handle_(win), render_(std::make_unique<dx9::Dx9Render>(win)) {
  
}

ViewControl::~ViewControl() {}

int ViewControl::Initialize() {
  // TODO: Define error return type
  if (render_->InitGraphics()) {
    xmedia::log::WriteLog("xmedia", xmedia::log::Level::Info,
                          "success to init graphics.");
    return 0;
  }
  xmedia::log::WriteLog("xmedia", xmedia::log::Level::Err,
                        "failed to init graphics.");
  return -1;
}

int ViewControl::CreateSubView(const ViewPosition& position) {
  std::lock_guard<std::mutex> lck(view_lock_);

  int boxId = generateGuid();
  view_box_map_.insert(
      std::make_pair(boxId, std::make_unique<ViewBox>(position)));
  return boxId;
}

void ViewControl::RemoveSubView(int id) {
  std::lock_guard<std::mutex> lck(view_lock_);
  view_box_map_.erase(id);
}

int ViewControl::AddViewSource(int box_id, const char* url, const char* user,
                               const char* password, bool useTCP) {
  std::lock_guard<std::mutex> lck(view_lock_);

  auto targetBox = view_box_map_.find(box_id);
  if (targetBox != view_box_map_.end()) {
    targetBox->second->AddViewSource(
        SourceConnection(url, user, password, useTCP));
    return box_id;
  }
  return -1;
}

void ViewControl::RemoveViewSource(int box_id) {
  std::lock_guard<std::mutex> lck(view_lock_);
  auto targetBox = view_box_map_.find(box_id);
  if (targetBox != view_box_map_.end()) {
    targetBox->second->RemoveViewSource();
    targetBox->second->SetType(BoxType::Empty);
  }
}

void* ViewControl::GetBackBuffer() { return (void*)render_->BackBuffer(); }

void ViewControl::DisplayView() {
  std::lock_guard<std::mutex> lck(view_lock_);
  // std::unique_lock<std::mutex> lck(_mtx);
  //_cv.wait(lck, [&] { return _viewBoxes.size() > 0; });
  render_->Display(view_box_map_);
}

void ViewControl::PresentView(HWND overrideWindow) {
  std::lock_guard<std::mutex> lck(view_lock_);
  // std::unique_lock<std::mutex> lck(_mtx);
  //_cv.wait(lck, [&] { return _viewBoxes.size() > 0; });
  render_->Display(view_box_map_);
  render_->Present(NULL, NULL, overrideWindow, NULL);
}

int ViewControl::generateGuid() {
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<int> dis(0, 65535);

  int generatedId = dis(gen);
  auto existedId =
      std::find_if(view_box_id_list_.cbegin(), view_box_id_list_.cend(),
                   [generatedId](int id) { return id == generatedId; });

  if (existedId == view_box_id_list_.end()) {
    view_box_id_list_.push_back(generatedId);
    return generatedId;
  }
  generateGuid();  // is it okay...?
}

}  // namespace player
}  // namespace xmedia