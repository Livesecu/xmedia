#include "render_manager.h"

#include "util/util.h"
#include "video_renderer.h"

constexpr static TCHAR app_name[] = TEXT("D3DImageSample");
typedef HRESULT(WINAPI* DIRECT3DCREATE9EXFUNCTION)(UINT SDKVersion,
                                                   IDirect3D9Ex**);

RenderManager::RenderManager()
    : d3d_(nullptr),
      d3d_ex_(nullptr),
      adapters_(0),
      renderers_(nullptr),
      current_renderer_(nullptr),
      hwnd_(nullptr),
      width_(1024),
      height_(1024),
      num_samples_(0),
      use_alpha_(false),
      surface_settings_changed_(true) {}

RenderManager::~RenderManager() {
  DestroyResources();
  if (hwnd_) {
    DestroyWindow(hwnd_);
    UnregisterClass(app_name, NULL);
  }
}

HRESULT RenderManager::Create(RenderManager** manager) {
  HRESULT hr = S_OK;
  *manager = new RenderManager();
  IFCOOM(*manager);

Cleanup:
  return hr;
}

// HRESULT RenderManager::EnsureDevices() {
//
//}
HRESULT RenderManager::Render() {
  return current_renderer_ ? current_renderer_->Render() : S_OK;
}

void RenderManager::set_size(UINT width, UINT height) {
  if (width_ != width || height_ != height) {
    width_ = width;
    height_ = height;
    surface_settings_changed_ = true;
  }
}
void RenderManager::set_alpha(bool use_alpha) {
  if (use_alpha_ != use_alpha) {
    use_alpha_ = use_alpha;
    surface_settings_changed_ = true;
  }
}
void RenderManager::set_num_desired_samples(UINT num_samples) {
  if (num_samples_ != num_samples) {
    num_samples_ = num_samples;
    surface_settings_changed_ = true;
  }
}

void RenderManager::set_adapter(POINT screenspace_point) {
  CleanupInvalidDevices();
  if (d3d_ && renderers_) {
    HMONITOR hMon = MonitorFromPoint(screenspace_point, MONITOR_DEFAULTTONULL);

    for (UINT i = 0; i < adapters_; ++i) {
      if (hMon == d3d_->GetAdapterMonitor(i)) {
        current_renderer_ = renderers_[i];
        break;
      }
    }
  }
}

HRESULT RenderManager::EnsureRenderers() {
  HRESULT hr = S_OK;

  if (!renderers_) {
    IFC(EnsureHwnd());

    assert(adapters_);
    renderers_ = new Renderer*[adapters_];
    IFCOOM(renderers_);
    SecureZeroMemory(renderers_, adapters_ * sizeof(renderers_[0]));

    for (UINT i = 0; i < adapters_; ++i) {
      //IFC(TriangleRenderer::Create(d3d_, d3d_ex_, hwnd_, i, &renderers_[i]));
      IFC(VideoRenderer::Create(d3d_, d3d_ex_, hwnd_, i, &renderers_[i]));
    }
    current_renderer_ = renderers_[0];
  }

Cleanup:
  return hr;
}

HRESULT RenderManager::EnsureHwnd() {
  HRESULT hr = S_OK;
  if (!hwnd_) {
    WNDCLASS wndclass;

    wndclass.style = CS_HREDRAW | CS_VREDRAW;
    wndclass.lpfnWndProc = DefWindowProc;
    wndclass.cbClsExtra = 0;
    wndclass.cbWndExtra = 0;
    wndclass.hInstance = NULL;
    wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
    wndclass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
    wndclass.lpszMenuName = NULL;
    wndclass.lpszClassName = app_name;

    if (!RegisterClass(&wndclass)) {
      IFC(E_FAIL);
    }

    hwnd_ = CreateWindow(app_name, TEXT("D3DImageSample"), WS_OVERLAPPEDWINDOW,
                         0,  // Initial X
                         0,  // Initial Y
                         0,  // Width
                         0,  // Height
                         NULL, NULL, NULL, NULL);
  }
Cleanup:
  return hr;
}

HRESULT RenderManager::EnsureD3DObjects() {
  HRESULT hr = S_OK;

  HMODULE d3d = NULL;
  if (!d3d_) {
    d3d = LoadLibrary(TEXT("d3d9.dll"));
    DIRECT3DCREATE9EXFUNCTION pfnCreate9Ex =
        (DIRECT3DCREATE9EXFUNCTION)GetProcAddress(d3d, "Direct3DCreate9Ex");
    if (pfnCreate9Ex) {
      IFC((*pfnCreate9Ex)(D3D_SDK_VERSION, &d3d_ex_));
      IFC(d3d_ex_->QueryInterface(__uuidof(IDirect3D9),
                                  reinterpret_cast<void**>(&d3d_)));
    } else {
      d3d_ = Direct3DCreate9(D3D_SDK_VERSION);
      if (!d3d_) {
        IFC(E_FAIL);
      }
    }
    adapters_ = d3d_->GetAdapterCount();
  }

Cleanup:
  if (d3d) FreeLibrary(d3d);
  return hr;
}

HRESULT RenderManager::TestSurfaceSettings() {
  HRESULT hr = S_OK;
  D3DFORMAT fmt = use_alpha_ ? D3DFMT_A8R8G8B8 : D3DFMT_X8R8G8B8;

  for (UINT i = 0; i < adapters_; ++i) {
    // Can we get HW rendering?
    IFC(d3d_->CheckDeviceType(i, D3DDEVTYPE_HAL, D3DFMT_X8R8G8B8, fmt, TRUE));

    // Is the format okay?
    IFC(d3d_->CheckDeviceFormat(
        i, D3DDEVTYPE_HAL, D3DFMT_X8R8G8B8,
        D3DUSAGE_RENDERTARGET |
            D3DUSAGE_DYNAMIC,  // We'll use dynamic when on XP
        D3DRTYPE_SURFACE, fmt));

    // D3DImage only allows multisampling on 9Ex devices. If we can't
    // multisample, overwrite the desired number of samples with 0.
    if (d3d_ex_ && num_samples_ > 1) {
      assert(num_samples_ <= 16);

      if (FAILED(d3d_->CheckDeviceMultiSampleType(
              i, D3DDEVTYPE_HAL, fmt, TRUE,
              static_cast<D3DMULTISAMPLE_TYPE>(num_samples_), NULL))) {
        num_samples_ = 0;
      }
    } else {
      num_samples_ = 0;
    }
  }
Cleanup:
  return hr;
}

void RenderManager::CleanupInvalidDevices() {
  for (UINT i = 0; i < adapters_; ++i) {
    if (FAILED(renderers_[i]->CheckDeviceState())) {
      DestroyResources();
      break;
    }
  }
}

void RenderManager::DestroyResources() {
  SAFE_RELEASE(d3d_);
  SAFE_RELEASE(d3d_ex_);

  for (UINT i = 0; i < adapters_; ++i) {
    delete renderers_[i];
  }
  delete[] renderers_;
  renderers_ = nullptr;
  current_renderer_ = nullptr;
  adapters_ = 0;
  surface_settings_changed_ = true;
}

HRESULT RenderManager::backbuffer_noref(IDirect3DSurface9** surface) {
  HRESULT hr = S_OK;
  *surface = NULL;

  CleanupInvalidDevices();
  IFC(EnsureD3DObjects());
  IFC(EnsureRenderers());

  if (surface_settings_changed_) {
    if (FAILED(TestSurfaceSettings())) IFC(E_FAIL);

    for (UINT i = 0; i < adapters_; ++i) {
      IFC(renderers_[i]->CreateSurface(width_, height_, use_alpha_,
                                       num_samples_));
      surface_settings_changed_ = false;
    }
  }
  if (current_renderer_) *surface = current_renderer_->surface_noref();

Cleanup:
  if (hr == D3DERR_DEVICELOST) hr = S_OK;
  return hr;
}

int AddViewContainer(std::unique_ptr<ViewContainer> view_container)
{
  return 0;
}