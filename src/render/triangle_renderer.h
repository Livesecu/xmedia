#pragma once

#include "renderer.h"

class TriangleRenderer : public Renderer {
 public:
  static HRESULT Create(IDirect3D9* d3d, IDirect3D9Ex* d3d_ex, HWND hwnd,
                        UINT adapter, Renderer** renderer);
  ~TriangleRenderer() override;

  HRESULT Render();

 protected:
  HRESULT Init(IDirect3D9* d3d, IDirect3D9Ex* d3d_ex, HWND hwnd, UINT adapter) override;

 private:
  TriangleRenderer();
  IDirect3DVertexBuffer9* vb_;
};
