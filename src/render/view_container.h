#pragma once

#include "video_source.h"

struct ViewSize {
  unsigned width;
  unsigned height;
};

struct ViewPosition {
  unsigned x;
  unsigned y;
};

class ViewContainer {
 public:
  ViewContainer(ViewPosition pos, ViewSize size,
                std::shared_ptr<VideoSource> video_source) {}
  ~ViewContainer() {}


 private:
  ViewPosition pos_;
  ViewSize size_;

  std::shared_ptr<VideoSource> video_source_;
};
