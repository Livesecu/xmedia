#pragma once

#include <memory>
#include <mutex>
#include <thread>
#include <unordered_map>
#include <vector>

#include "player_commons.h"
#include "render.h"
#include "view_box.h"

namespace xmedia {
namespace player {

class ViewControl {
 public:
  explicit ViewControl(HWND win);
  ~ViewControl();

  // Must be called for the view control to check graphic compatibililty.
  int Initialize();

  // Control views
  int CreateSubView(const ViewPosition& position);
  void RemoveSubView(int id);
  int AddViewSource(int boxid, const char* url, const char* user,
                    const char* password, bool useTCP);
  void RemoveViewSource(int box_id);

  // Render functions
  void* GetBackBuffer();
  void DisplayView();
  void PresentView(HWND overrideWindow);

 private:
  int generateGuid();

  HWND win_handle_;
  std::unique_ptr<Render> render_;

  std::unordered_map<int, std::unique_ptr<ViewBox>> view_box_map_;

  std::mutex view_lock_;
  std::vector<int> view_box_id_list_;
};

}  // namespace player
}  // namespace xmedia