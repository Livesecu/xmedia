#include "dx9_render.h"

#include <iostream>

#include "dll/xmedia.h"
#include "log/logger.h"
#include "view_box.h"

namespace xmedia {
namespace player {

namespace dx9 {

Dx9Render::Dx9Render(HWND win)
    : _d3d(Direct3DCreate9(D3D_SDK_VERSION)),
      _dev(nullptr),
      _offscreenSurface(nullptr),
      _backBuffer(nullptr),
      _displayModes(nullptr),
      _resizer(nullptr),
      _adapterCount(0),
      _win(win),
      _width(0),
      _height(0) {
  if (!_win) {
    WNDCLASS wndclass;

    wndclass.style = CS_HREDRAW | CS_VREDRAW;
    wndclass.lpfnWndProc = DefWindowProc;
    wndclass.cbClsExtra = 0;
    wndclass.cbWndExtra = 0;
    wndclass.hInstance = NULL;
    wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
    wndclass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
    wndclass.lpszMenuName = NULL;
    wndclass.lpszClassName = L"render window";

    if (RegisterClass(&wndclass)) {
      _win = CreateWindow(L"render window", NULL, WS_OVERLAPPEDWINDOW,
                          0,  // Initial X
                          0,  // Initial Y
                          0,  // Width
                          0,  // Height
                          NULL, NULL, NULL, NULL);
    }
    // log::api::WriteLog(PLAYER_LOGGER, log::Level::Info,"Dummy window is
    // created");
  }
  _resizer = new xmedia::core::utils::ImageConverter();
}

Dx9Render::~Dx9Render() {
  if (_displayModes) {
    delete[] _displayModes;
    _displayModes = nullptr;
  }

  if (_resizer) {
    delete _resizer;
    _resizer = nullptr;
  }
  xmedia::log::WriteLog("xmedia", xmedia::log::Level::Debug,
                        "dx9 render is destroyed.");
}

bool Dx9Render::InitGraphics() {
  if (EnsureDevice() && EnsureFormatConversion()) {
    if (FAILED(
            _dev->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &_backBuffer))) {
      xmedia::log::WriteLog("xmedia", xmedia::log::Level::Err,
                            "failed to get backbuffer.");
      return false;
    }

    if (FAILED(_dev->SetRenderTarget(0, _backBuffer))) {
      xmedia::log::WriteLog("xmedia", xmedia::log::Level::Err,
                            "failed to set render target.");
      return false;
    }

    // TODO: Have to be moved.
    if (FAILED(_dev->CreateOffscreenPlainSurface(_width, _height, D3DFMT_YV12,
                                                 D3DPOOL_DEFAULT,
                                                 &_offscreenSurface, NULL))) {
      // log::api::WriteLog(PLAYER_LOGGER, log::Level::Err, "Failed to create a
      // YV12 surface"); log::api::WriteLog(PLAYER_LOGGER, log::Level::Err, "\t
      // (%d x %d)", _width, _height);
      return false;
    }

    //// TODO: 이미지당 텍스쳐가 모두 만들어져야 하는지 확인!
    // if (FAILED(D3DXCreateTextureFromFile(_dev, LogoImagePath, &_texture))) {
    //  //log::api::WriteLog(PLAYER_LOGGER, log::Level::Err, "Failed to create a
    //  image texture");
    //  //log::api::WriteLog(PLAYER_LOGGER, log::Level::Err, "\t image path:
    //  [%s]",LogoImagePath); return false;
    //}

    //// TODO: spirte의 정확한 역할 확인!!
    // if (FAILED(D3DXCreateSprite(_dev, &_sprite))) {
    //  //log::api::WriteLog(PLAYER_LOGGER, log::Level::Err, "Failed to create a
    //  sprite"); return false;
    //}

    // log::api::WriteLog(PLAYER_LOGGER, log::Level::Info, "Complete ensuring
    // d3d graphics");
    return true;
  }

  // log::api::WriteLog(PLAYER_LOGGER, log::Level::Err, "Failed to ensure d3d
  // graphics");

  return false;
}

bool Dx9Render::EnsureDevice() {
  _adapterCount = _d3d->GetAdapterCount();

  _displayModes = new D3DDISPLAYMODE[_adapterCount];
  ZeroMemory(_displayModes, _adapterCount * sizeof(D3DDISPLAYMODE));

  for (unsigned i = 0; i < _adapterCount; ++i) {
    _d3d->GetAdapterDisplayMode(i, &_displayModes[i]);
  }

  // NOTE: Primary monitor will be used only now.
  auto monitor = _d3d->GetAdapterMonitor(D3DADAPTER_DEFAULT);
  _monitor.cbSize = sizeof(MONITORINFO);
  if (GetMonitorInfo(monitor, &_monitor)) {
    _width = _monitor.rcMonitor.right - _monitor.rcMonitor.left;
    _height = _monitor.rcMonitor.bottom - _monitor.rcMonitor.top;
  } else {
    xmedia::log::WriteLog("xmedia", xmedia::log::Level::Err,
                          "failed to get monitor info.");
    _width = 1920;
    _height = 1080;
  }

  D3DPRESENT_PARAMETERS d3dpp;
  ZeroMemory(&d3dpp, sizeof(d3dpp));

  d3dpp.Windowed = TRUE;
  d3dpp.hDeviceWindow = _win;
  d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
  d3dpp.BackBufferWidth = _width;
  d3dpp.BackBufferHeight = _height;
  d3dpp.BackBufferFormat = _displayModes[0].Format;

  delete[] _displayModes;

  xmedia::log::WriteLog("xmedia", xmedia::log::Level::Info,
                        "present parameters: ");
  xmedia::log::WriteLog("xmedia", xmedia::log::Level::Info,
                        "\t backbuffer with: %d", d3dpp.BackBufferWidth);
  xmedia::log::WriteLog("xmedia", xmedia::log::Level::Info,
                        "\t backbuffer height: %d", d3dpp.BackBufferHeight);
  if (d3dpp.BackBufferFormat == D3DFMT_X8R8G8B8) {
    xmedia::log::WriteLog("xmedia", xmedia::log::Level::Info,
                          "\t backbuffer format: X8R8G8B8");
  } else if (d3dpp.BackBufferFormat == D3DFMT_X8B8G8R8) {
    xmedia::log::WriteLog("xmedia", xmedia::log::Level::Info,
                          "\t backbuffer format: X8B8G8R8");
  } else if (d3dpp.BackBufferFormat == D3DFMT_A8R8G8B8) {
    xmedia::log::WriteLog("xmedia", xmedia::log::Level::Info,
                          "\t backbuffer format: A8R8G8B8");
  } else if (d3dpp.BackBufferFormat == D3DFMT_A8B8G8R8) {
    xmedia::log::WriteLog("xmedia", xmedia::log::Level::Info,
                          "\t backbuffer format: A8B8G8R8");
  } else {
    xmedia::log::WriteLog("xmedia", xmedia::log::Level::Warn,
                          "\t backbuffer format: Unknown[%d]",
                          d3dpp.BackBufferFormat);
  }

  D3DCAPS9 caps;
  DWORD dwVertexProcessing;
  if (FAILED(_d3d->GetDeviceCaps(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, &caps))) {
    xmedia::log::WriteLog("xmedia", xmedia::log::Level::Info,
                          "failed to get device capabilities.");
    return false;
  }

  if ((caps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT) ==
      D3DDEVCAPS_HWTRANSFORMANDLIGHT) {
    dwVertexProcessing = D3DCREATE_HARDWARE_VERTEXPROCESSING;
    xmedia::log::WriteLog("xmedia", xmedia::log::Level::Info,
                          "use hardware vertex processing.");
  } else {
    dwVertexProcessing = D3DCREATE_SOFTWARE_VERTEXPROCESSING;
    xmedia::log::WriteLog("xmedia", xmedia::log::Level::Info,
                          "use software vertex processing.");
  }

  if (FAILED(_d3d->CheckDeviceType(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL,
                                   d3dpp.BackBufferFormat,
                                   d3dpp.BackBufferFormat, TRUE))) {
    xmedia::log::WriteLog("xmedia", xmedia::log::Level::Err,
                          "failed to check device type");
    return false;
  }

  if (FAILED(_d3d->CreateDevice(
          D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, _win,
          dwVertexProcessing | D3DCREATE_MULTITHREADED | D3DCREATE_FPU_PRESERVE,
          &d3dpp, &_dev))) {
    xmedia::log::WriteLog("xmedia", xmedia::log::Level::Err,
                          "failed to create d3d device.");
    return false;
  }
  return true;
}

bool Dx9Render::EnsureFormatConversion() {
  // TODO.
  return true;
}

void Dx9Render::Display(
    const std::unordered_map<int, std::unique_ptr<ViewBox>>& viewBoxMap) {
  _dev->ColorFill(_offscreenSurface, NULL, D3DCOLOR_XYUV(0, 128, 128));
  _dev->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0L);

  for (const auto& view : viewBoxMap) {
    auto viewBox = view.second.get();
    const auto& pos = viewBox->GetViewPos();

    if (viewBox->GetType() == BoxType::Video) {
      auto imageSrc = viewBox->Source();
      if (imageSrc) {
        auto frame = imageSrc->GetFrame();
        if (!frame) continue;

        uint8_t* resizedData[4];
        int resizedDataSize[4];

        auto initIndex = &resizedData[0];
        auto ret = _resizer->Rescale(frame, resizedData, resizedDataSize, pos.w,
                                     pos.h, PixelFormat::YV12);
        if (ret) {
#ifdef USE_YUV_DEBUG
          char buf[1024];
          const char* filename = "D:\\output\\yuv_out";
          snprintf(buf, sizeof(buf), "%s-%d.yuv", filename,
                   frame->avFrame->coded_picture_number);
          FILE* testFile = fopen(buf, "wb");

          if (testFile) {
            fwrite(resizedData[0], 1, ret, testFile);
            fclose(testFile);
          }
#endif
          FillImageBuffer(viewBox->GetViewPos(), resizedData[0], resizedData[1],
                          resizedData[2], resizedDataSize[0],
                          resizedDataSize[1]);

          av_freep(initIndex);
        }
        delete frame;
        frame = nullptr;
      }
    }
  }
  if (FAILED(_dev->StretchRect(_offscreenSurface, NULL, _backBuffer, NULL,
                               D3DTEXF_NONE))) {
    xmedia::log::WriteLog("xmedia", xmedia::log::Level::Err,
                          "failed to copy offscreen surface to backbuffer");
    return;
  }

  // draw image, font, line and so on.
  if (SUCCEEDED(_dev->BeginScene())) {
    for (const auto& view : viewBoxMap) {
      if (view.second->GetType() == BoxType::Empty) {
        DrawLogoImage(view.second->GetViewPos());
      }
    }
    _dev->EndScene();
  }
}

void Dx9Render::Present(RECT* pSourceRect, RECT* pDestRect,
                        HWND hDestWindowOverride, RGNDATA* pDirtyRegion) {
  _dev->Present(pSourceRect, pDestRect, hDestWindowOverride, pDirtyRegion);
}

void Dx9Render::GetDisplayMode() {
  _displayModes = new D3DDISPLAYMODE[_adapterCount];
  ZeroMemory(_displayModes, _adapterCount * sizeof(D3DDISPLAYMODE));

  for (unsigned i = 0; i < _adapterCount; ++i) {
    _d3d->GetAdapterDisplayMode(i, &_displayModes[i]);
  }
}

void Dx9Render::FillImageBuffer(const ViewPosition& pos, uint8_t* y, uint8_t* u,
                                uint8_t* v, int ySize, int uvSize) {
  if (y && u && v) {
    D3DLOCKED_RECT lockedRect;
    _offscreenSurface->LockRect(&lockedRect, NULL, NULL);

    auto pitch = lockedRect.Pitch;
    uint8_t* pic = (uint8_t*)lockedRect.pBits;

    auto startPtr = pic;
    startPtr += pitch * (int)pos.y;
    for (int i = 0; i < pos.h; ++i) {
      memcpy(startPtr + (int)pos.x, y, ySize);
      startPtr += pitch;
      y += ySize;
    }

    pic += pitch * _height;
    startPtr = pic;
    startPtr += (int)std::ceil(pitch / 2) * (int)std::ceil(pos.y / 2);
    for (int i = 0; i < pos.h / 2; ++i) {
      memcpy(startPtr + (int)pos.x / 2, v, uvSize);
      startPtr += pitch / 2;
      v += uvSize;
    }

    pic += (int)std::ceil(pitch / 2) * (int)std::ceil(_height / 2);
    startPtr = pic;
    startPtr += (int)std::ceil(pitch / 2) * (int)std::ceil(pos.y / 2);
    for (int i = 0; i < pos.h / 2; ++i) {
      memcpy(startPtr + (int)pos.x / 2, u, uvSize);
      startPtr += pitch / 2;
      u += uvSize;
    }

    _offscreenSurface->UnlockRect();
  }
}

void Dx9Render::DrawLogoImage(const ViewPosition& vp) {
  if (!_sprite) {
    // error check to load an image
    return;
  }

  if (SUCCEEDED(_sprite->Begin(D3DXSPRITE_ALPHABLEND |
                               D3DXSPRITE_DO_NOT_ADDREF_TEXTURE))) {
    // TODO: need to set proper position....
    D3DXMATRIX mat, mat2;
    D3DXMatrixMultiply(
        &mat, D3DXMatrixScaling(&mat, 0.5f, 0.5f, 0.0f),
        D3DXMatrixTranslation(&mat2, (float)vp.x, (float)vp.y, 0.0f));
    _sprite->SetTransform(&mat);

    _sprite->Draw(_texture, NULL, NULL, NULL, 0xFFFFFFFF);
    _sprite->End();
  }
}

}  // namespace dx9
}  // namespace player
}  // namespace xmedia