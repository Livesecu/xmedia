#include "view_box.h"
#include "log/logger.h"

namespace xmedia {
namespace player {

ViewBox::ViewBox(const ViewPosition& vp)
    : view_position_(vp), box_type_(BoxType::Empty), source_(nullptr) {
  xmedia::log::WriteLog("xmedia", xmedia::log::Level::Debug,
                        "[%d] new view box is created.",
                        std::this_thread::get_id());
}

ViewBox::~ViewBox() {
  xmedia::log::WriteLog("xmedia", xmedia::log::Level::Debug,
                        "[%d] view box is destroyed.",
                        std::this_thread::get_id());
}

void ViewBox::AddViewSource(const SourceConnection& connection) {
  // TODO: 이미 할당된 소스가 있는경우는 어떻게 처리?
  //       변경? 변경한다면 타이밍은?
  if (source_) {
    xmedia::log::WriteLog("xmedia", xmedia::log::Level::Warn,
                          "view source is already allocated.");
    return;
  } else {
    source_ = new ViewSource(connection);
    box_type_ = BoxType::Video;
  }
}

void ViewBox::RemoveViewSource() {
  if (source_) {
    delete source_;
    source_ = nullptr;
  }
  box_type_ = BoxType::Empty;
}

}  // namespace player
}  // namespace xmedia