#pragma once

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#ifdef XMEDIA_EXPORTS
#define XMEDIA_API __declspec(dllexport)
#else
#define XMEDIA_API __declspec(dllimport)
#endif

extern "C" {
/* Warning: Do not use this. Preparing a new version! */
XMEDIA_API HRESULT WINAPI SetSize(UINT width, UINT height);
XMEDIA_API HRESULT WINAPI SetAlpha(BOOL use_alpha);
XMEDIA_API HRESULT WINAPI SetNumDesiredSamples(UINT num_samples);
XMEDIA_API HRESULT WINAPI SetAdapter(POINT screenspace_point);
XMEDIA_API HRESULT WINAPI GetBackBufferNoRef(void** surface);
XMEDIA_API HRESULT WINAPI Render();
XMEDIA_API void WINAPI Destroy();
XMEDIA_API HRESULT WINAPI CreateViewContainer(int container_id, UINT x, UINT y,
                                           UINT width, UINT height);
XMEDIA_API HRESULT WINAPI CreateVideoSource(const char* url, const char* user,
                                              const char* password,
                                              bool use_tcp);


XMEDIA_API void __stdcall LoadXmediaPlayer(const char* log_path);

XMEDIA_API void __stdcall CreateViewControl(HWND wnd);
XMEDIA_API void __stdcall ReleaseViewControl();

XMEDIA_API int __stdcall InitializeViewControl();

XMEDIA_API int __stdcall CreateSubView(double x, double y, double w, double h,
                                       double border);
XMEDIA_API void __stdcall RemoveSubView(int boxid);
XMEDIA_API void __stdcall AddViewSource(int boxid, const char* url,
                                        const char* user, const char* password,
                                        bool useTCP);
XMEDIA_API void __stdcall RemoveViewSource(int boxid);
XMEDIA_API void __stdcall GetBackBuffer(void** backbuffer);
XMEDIA_API void __stdcall Display();
XMEDIA_API void __stdcall Present(HWND overrideWindow);

/* Test API */
XMEDIA_API void WINAPI TestOpenStream(const char* url, const char* user, const char* password, void** stream);
XMEDIA_API void WINAPI TestCloseStream(void* stream);

}

#ifdef UNICODE
static const wchar_t* LogoImagePath = L".\\Assets\\Images\\Rpider_Logo.png";
// static const wchar_t* LogoImagePath = L"..\\..\\resources\\Rpider_Logo.png";
#else
static const wchar_t* LogoImagePath = ".\\Assets\\Images\\Rpider_Logo.png";
// static const wchar_t* LogoImagePath = L"..\\..\\resources\\Rpider_Logo.png";

#endif  // UNICODE