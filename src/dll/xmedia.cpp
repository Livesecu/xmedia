#include "xmedia.h"

#include "log/logger.h"
#include "render/dx9_render.h"
#include "render/render_manager.h"
#include "render/view_control.h"
#include "render/view_source.h"
#include "util/util.h"

static RenderManager* manager = nullptr;
static xmedia::player::ViewControl* view_control = nullptr;

static HRESULT EnsureRenderManager() {
  return manager ? S_OK : RenderManager::Create(&manager);
}

/* Render API */
XMEDIA_API HRESULT WINAPI SetSize(UINT width, UINT height) {
  HRESULT hr = S_OK;
  IFC(EnsureRenderManager());
  manager->set_size(width, height);
Cleanup:
  return hr;
}

XMEDIA_API HRESULT WINAPI SetAlpha(BOOL use_alpha) {
  HRESULT hr = S_OK;
  IFC(EnsureRenderManager());
  manager->set_alpha(use_alpha);
Cleanup:
  return hr;
}

XMEDIA_API HRESULT WINAPI SetNumDesiredSamples(UINT num_samples) {
  HRESULT hr = S_OK;
  IFC(EnsureRenderManager());
  manager->set_num_desired_samples(num_samples);
Cleanup:
  return hr;
}

XMEDIA_API HRESULT WINAPI SetAdapter(POINT screenspace_point) {
  HRESULT hr = S_OK;
  IFC(EnsureRenderManager());
  manager->set_adapter(screenspace_point);
Cleanup:
  return hr;
}

XMEDIA_API HRESULT WINAPI GetBackBufferNoRef(void** surface) {
  HRESULT hr = S_OK;
  IFC(EnsureRenderManager());
  IFC(manager->backbuffer_noref((IDirect3DSurface9**)surface));
Cleanup:
  return hr;
}

XMEDIA_API HRESULT WINAPI Render() {
  assert(manager);
  return manager->Render();
}

XMEDIA_API void WINAPI Destroy() {
  delete manager;
  manager = nullptr;
}

XMEDIA_API HRESULT WINAPI CreateVideoSource(const char* url, const char* user,
                                            const char* password,
                                            bool use_tcp) {
  HRESULT hr = S_OK;
  /*auto video_source = std::make_shared<VideoSource>(
      std::make_unique<VideoConnection>(url, user, password, use_tcp));*/

  return hr;
}

XMEDIA_API
void __stdcall LoadXmediaPlayer(const char* log_path) {
  xmedia::log::CreateLogger("xmedia", log_path, "xmedia", xmedia::log::Level::Debug);
}

XMEDIA_API
void __stdcall CreateViewControl(HWND wnd) {
  view_control = new xmedia::player::ViewControl(wnd);
}

XMEDIA_API
void __stdcall ReleaseViewControl() {
  if (view_control) {
    delete view_control;
    view_control = nullptr;
  }
}

XMEDIA_API
int __stdcall InitializeViewControl() {
  if (view_control) return view_control->Initialize();
  return -1;
}

XMEDIA_API
int __stdcall CreateSubView(double x, double y, double w, double h,
                            double border) {
  if (view_control) {
    return view_control->CreateSubView(
        xmedia::player::ViewPosition(x, y, w, h, border));
  }
  return -1;
}

XMEDIA_API
void __stdcall RemoveSubView(int boxid) {
  if (view_control) {
    view_control->RemoveSubView(boxid);
  }
}

XMEDIA_API
void __stdcall AddViewSource(int boxid, const char* url, const char* user,
                             const char* password, bool useTCP) {
  if (view_control) {
    view_control->AddViewSource(boxid, url, user, password, useTCP);
  }
}

XMEDIA_API
void __stdcall RemoveViewSource(int boxid) {
  if (view_control) {
    view_control->RemoveViewSource(boxid);
  }
}

XMEDIA_API
void __stdcall GetBackBuffer(void** backbuffer) {
  if (view_control) {
    *backbuffer = view_control->GetBackBuffer();
  }
}

XMEDIA_API
void __stdcall Display() {
  if (view_control) {
    view_control->DisplayView();
  }
}

XMEDIA_API
void __stdcall Present(HWND overrideWindow) {
  if (view_control) {
    view_control->PresentView(overrideWindow);
  }
}

XMEDIA_API void WINAPI TestOpenStream(const char* url, const char* user,
                                      const char* password, void** stream) {
  auto new_stream = xmedia::core::NetworkStreamReader::Create(url);
  if (new_stream) {
    if (new_stream->OpenStream(url, user, password, false))
      *stream = new_stream;
  }
}

XMEDIA_API void WINAPI TestCloseStream(void* stream) {
  if (stream) {
    auto close_stream = static_cast<xmedia::core::NetworkStreamReader*>(stream);
    close_stream->CloseStream();
  }
}