#pragma once

#include "core/core_commons.h"

namespace xmedia {

namespace core {
namespace window {

using WindowProcedureFunc = LRESULT WINAPI(HWND window, unsigned int msg, WPARAM wp, LPARAM lp);

class SimpleWindow
{
protected:
    WindowHandle _handle;
    int _width;
    int _height;

public:
    SimpleWindow();
    virtual ~SimpleWindow();

    SimpleWindow(const SimpleWindow& other) = delete;
    SimpleWindow& operator=(const SimpleWindow& other) = delete;

    void CreateEmptyWindow();
    void CreateDefaultWindow();

    virtual void MessageLoop();

    WindowHandle Get() { return _handle; }
    int Width() { return _width; }
    int Height() { return _height; }

    // TODO: 얘는 어떻게 처리해야하지...?
    //static LRESULT WINAPI WindowProcedure(HWND window, unsigned int msg, WPARAM wp, LPARAM lp);

private:
    void registerClass(WindowProcedureFunc func);
    WindowHandle createWindow(int x, int y, int w, int h);
};

}   // window
}   // core

}   // namespace livemediacore