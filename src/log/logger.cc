#include "logger.h"

#include <stdarg.h>

#include <filesystem>

#include "spdlog/async.h"
#include "spdlog/async_logger.h"
#include "spdlog/sinks/daily_file_sink.h"
#include "spdlog/sinks/wincolor_sink.h"
#include "spdlog/spdlog.h"

namespace xmedia {
namespace log {

static bool IsInitializedThreadPool = false;

void CreateLogger(const std::string& name, const std::string& directory,
                  const std::string& file_name, Level level) {
  try {
    if (!IsInitializedThreadPool) {
      spdlog::init_thread_pool(512, 1);
      IsInitializedThreadPool = true;
    }

    if (!std::filesystem::exists(directory)) {
      fprintf(stderr, "Create directory..[%s]\n", directory.c_str());

      std::filesystem::create_directories(directory);
    }

#ifdef _WIN32
    auto stdout_sink =
        std::make_shared<spdlog::sinks::wincolor_stdout_sink_mt>();
    // // with colored.
#else
    auto console_sink = std::make_shared<spdlog::sinks::stdout_sink_mt>();
    sinks.push_back(std::make_shared<spdlog::sinks::ansicolor_sink>(
        console_sink));  // with colored.
#endif
    auto daily_file_sink = std::make_shared<spdlog::sinks::daily_file_sink_mt>(
        directory + file_name + ".log", 23, 59);  // 23:59

    std::vector<spdlog::sink_ptr> sinks{daily_file_sink, stdout_sink};
    //std::vector<spdlog::sink_ptr> sinks{stdout_sink};

    auto combined_logger = std::make_shared<spdlog::async_logger>(
        name, sinks.begin(), sinks.end(), spdlog::thread_pool(),
        spdlog::async_overflow_policy::block);
    combined_logger->set_pattern("[%Y-%m-%d %H:%M:%S.%e][%P][%l] %v");
    combined_logger->set_level(static_cast<spdlog::level::level_enum>(level));

    spdlog::register_logger(combined_logger);

    spdlog::flush_every(std::chrono::seconds(3));
  } catch (const spdlog::spdlog_ex& ex) {
    fprintf(stderr, "CreateLogger exception. (%s)\n", ex.what());
  }
}

void DestoyLogger() { spdlog::drop_all(); }

void WriteLog(const std::string& name, Level level, const char* format, ...) {
  std::string buffer;

  va_list ap, ap2;
  va_start(ap, format);
  va_copy(ap2, ap);
#ifdef _WIN32
  if (auto needbytes = _vscprintf(format, ap)) {
    buffer.resize(needbytes);
    vsnprintf(&buffer[0], needbytes + 1, format, ap2);
  }
#else
  if (auto needbytes = vsnprintf(nullptr, 0, format, ap)) {
    buffer.resize(needbytes + 1);
    vsnprintf(&buffer[0], needbytes + 1, format, ap2);
  }
#endif
  va_end(ap2);
  va_end(ap);

  switch (level) {
    case Level::Trace: {
      spdlog::get(name)->trace<const char*>(buffer.c_str());
      break;
    }
    case Level::Debug: {
      spdlog::get(name)->debug<const char*>(buffer.c_str());
      break;
    }
    case Level::Info: {
      spdlog::get(name)->info<const char*>(buffer.c_str());
      break;
    }
    case Level::Warn: {
      spdlog::get(name)->warn<const char*>(buffer.c_str());
      break;
    }
    case Level::Err: {
      spdlog::get(name)->error<const char*>(buffer.c_str());
      break;
    }
    case Level::Critical: {
      spdlog::get(name)->critical<const char*>(buffer.c_str());
      break;
    }
    default: {
      break;
    }
  }

  // spdlog::get("xlogger")->flush();  // Important: logger->flush() is always a
  // synchronous operation, even if the logger is in async mode!
}

}  // namespace log
}  // namespace xpider