#ifndef CORE_LOGGING_LOGGER_H
#define CORE_LOGGING_LOGGER_H

#include <string>

namespace xmedia {
namespace log {

enum Level {
  Trace,
  Debug,
  Info,
  Warn,
  Err,
  Critical,
};

void CreateLogger(const std::string& name, const std::string& directory,
                  const std::string& file_name, Level level = Level::Info);
void DestoyLogger();
void WriteLog(const std::string& name, Level level, const char* format, ...);

}  // namespace log
}  // namespace xpider

#endif