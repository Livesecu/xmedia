#pragma once

#include "decoder/core_commons.h"
#include "decoder/packet_queue.h"

namespace xmedia {
namespace core {
namespace sink {

class SinkFactory {
 public:
  static MediaSink* Create(UsageEnvironment& env, const char* codecName,
                           PacketQueue* queue);
};

}  // namespace sink
}  // namespace core
}  // namespace xmedia
