#pragma once

#include "decoder/packet_queue.h"
#include <atomic>

namespace xmedia {
namespace core {

class NetworkStreamReader {
 public:
  static NetworkStreamReader* Create(std::string_view url);
  virtual ~NetworkStreamReader();

  virtual bool OpenStream(std::string_view url, std::string_view user,
                          std::string_view password, bool useTcp) = 0;
  virtual void CloseStream() = 0;

  virtual Packet* ReadStream();

  virtual bool alive() { return alive_; }

 protected:
  NetworkStreamReader();

  std::string _url;
  std::string _user;
  std::string _password;

  PacketQueue* _packetQueue;

  std::atomic_bool alive_;
};

}  // namespace core
}  // namespace xmedia