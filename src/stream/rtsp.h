#pragma once

#include <thread>

#include "decoder/core_commons.h"
#include "network_stream.h"

namespace xmedia {
namespace core {

static const DWORD REQUEST_TIMEOUT = 50000;  // millisecond

enum class RTSPError {
  Options,
  Describe,
  Setup,
  Play,
  Teardown,
  Getparameter,
  Success,
  Timeout,
  Unkown,
};

// Forward Declaration
class CustomRTSPClient;

// This class run the evet loop for live555 with thread!
class RTSPStream : public NetworkStreamReader {
 public:
  RTSPStream();
  ~RTSPStream() override;

  bool OpenStream(std::string_view url, std::string_view user,
                  std::string_view password, bool useTcp) override;
  void CloseStream() override;

  RTSPStream(const RTSPStream& rhs) = delete;
  RTSPStream& operator=(const RTSPStream& rhs) = delete;

 private:
  static void EventLoop();

  static TaskScheduler* _sch;
  static UsageEnvironment* _env;
  static std::thread* _thr;

  CustomRTSPClient* _rtspClient;
};

class CustomRTSPClient : public RTSPClient {
 public:
  static CustomRTSPClient* Create(UsageEnvironment& env, PacketQueue* queue,
                                  char const* rtspURL, int verbosityLevel = 0,
                                  char const* applicationName = NULL,
                                  portNumBits tunnelOverHTTPPortNum = 0,
                                  int socketNumToServer = -1) {
    return new CustomRTSPClient(env, queue, rtspURL, verbosityLevel,
                                applicationName, tunnelOverHTTPPortNum,
                                socketNumToServer);
  }

  Authenticator* Auth;

  PacketQueue* Queue;

  MediaSession* Session;
  MediaSubsession* Subsession;
  MediaSubsessionIterator* SubsessionIter;

  TaskToken ResetTask;
  TaskToken DescribeTask;
  TaskToken KeepAliveTask;

  bool SupportGetParameter;
  bool UseTCP;

  int NextDescribeDelay;

  std::string RtspUrl;

  HANDLE CallbackEvent;

  RTSPError LastError;

 protected:
  CustomRTSPClient(UsageEnvironment& env, PacketQueue* queue,
                   char const* rtspURL, int verbosityLevel,
                   char const* applicationName,
                   portNumBits tunnelOverHTTPPortNum, int socketNumToServer);

  CustomRTSPClient(const CustomRTSPClient& rhs) = delete;
  CustomRTSPClient& operator=(const CustomRTSPClient& rhs) = delete;

  virtual ~CustomRTSPClient();

 public:
  bool SendOptions();
  bool SendDescribe();
  bool SendSetup();
  bool SendPlay();
  bool SendTeardown();
  // NOTE: Get parameter command will be scheduled in live555 event loop.
  bool SendGetparameter();

  void ScheduleDescribeCommand();
  void ScheduleLivenessCommand();
  static void ScheduleDescribe(void* clientData);
  static void ScheduleLiveness(void* clientData);

  static void OptionsCallback(RTSPClient* rtspClient, int resultCode,
                              char* resultString);
  static void DescribeCallback(RTSPClient* rtspClient, int resultCode,
                               char* resultString);
  static void SetupCallback(RTSPClient* rtspClient, int resultCode,
                            char* resultString);
  static void PlayCallback(RTSPClient* rtspClient, int resultCode,
                           char* resultString);
  static void TeardownCallback(RTSPClient* rtspClient, int resultCode,
                               char* resultString);
  static void GetParameterCallback(RTSPClient* rtspClient, int resultCode,
                                   char* resultString);

  // static void SetupSubsession(RTSPClient* rtspClient);

  // void SendDescribe();

  static void DoReset(void* clientData);
  void DoReset();
  void Reset();

  void ClearAllSession();

  /*void continueAfterLivenessCommand(int resultCode, bool supportGetParameter);
  static void sendLivenessCommand(void* clientData);*/

  void scheduleReset();
};

}  // namespace core
}  // namespace xmedia