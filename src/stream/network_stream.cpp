#include "network_stream.h"
#include "rtsp.h"
#include "log/logger.h"

constexpr int QUEUE_SIZE = 20;

namespace xmedia {
namespace core {

NetworkStreamReader* NetworkStreamReader::Create(std::string_view url) {
  if (url.find("rtsp://") != std::string::npos) {
    xmedia::log::WriteLog("xmedia", xmedia::log::Level::Debug,
                          "rtsp stream reader will be created.");
    return new RTSPStream();
  }
  return nullptr;
}

NetworkStreamReader::NetworkStreamReader()
    : _packetQueue(new PacketQueue(QUEUE_SIZE)) {}

NetworkStreamReader::~NetworkStreamReader() {
  if (_packetQueue) {
    delete _packetQueue;
    _packetQueue = nullptr;
  }
  xmedia::log::WriteLog("xmedia", xmedia::log::Level::Debug,
                        "network stream reader is destroyed.");
}

Packet* NetworkStreamReader::ReadStream() { return _packetQueue->Get(); }

}  // namespace core
}  // namespace xmedia