#include "rtsp.h"

#include <string>
#include <thread>

#include "log/logger.h"
#include "sink_factory.h"

constexpr DWORD SEND_TIMEOUT_MILLI = 1000;

namespace xmedia {
namespace core {

TaskScheduler* RTSPStream::_sch = nullptr;
UsageEnvironment* RTSPStream::_env = nullptr;

std::thread* RTSPStream::_thr = nullptr;

RTSPStream::RTSPStream() : _rtspClient(nullptr) {
  if (!_sch) {
    _sch = BasicTaskScheduler::createNew();
  }

  if (!_env) {
    _env = BasicUsageEnvironment::createNew(*_sch);
  }

  if (!_thr) {
    _thr = new std::thread(EventLoop);
    xmedia::log::WriteLog("xmedia", xmedia::log::Level::Debug,
                          "new event loop is created.");
  }
}

RTSPStream::~RTSPStream() {
  xmedia::log::WriteLog("xmedia", xmedia::log::Level::Debug,
                        "[%d] rtsp stream is destroyed.",
                        std::this_thread::get_id());
  /*if (_thr) {
      _thr->join();
      delete _thr;
      _thr = nullptr;
  }*/
}

void RTSPStream::EventLoop() { _env->taskScheduler().doEventLoop(); }

bool RTSPStream::OpenStream(std::string_view url, std::string_view user,
                            std::string_view password, bool useTcp) {
  xmedia::log::WriteLog("xmedia", xmedia::log::Level::Info,
                        "[%d] Try to open rtsp stream...", std::this_thread::get_id());

  _rtspClient = CustomRTSPClient::Create(*_env, _packetQueue, url.data(),
                                         RTSP_CLIENT_VERBOSITY_LEVEL);
  if (!_rtspClient) {
    xmedia::log::WriteLog("xmedia", xmedia::log::Level::Err,
                          "Failed to create a rtsp client");
    return false;
  }

  if (_rtspClient->url() == nullptr) {
    xmedia::log::WriteLog("xmedia", xmedia::log::Level::Err,
                          "rtsp url is empty.");
    return false;
  }

  if (!user.empty() && !password.empty()) {
    _rtspClient->Auth = new Authenticator(user.data(), password.data());
  }

  _rtspClient->UseTCP = useTcp;

  if (!_rtspClient->SendOptions()) {
    xmedia::log::WriteLog("xmedia", xmedia::log::Level::Err,
                          "failed to send rtsp OPTIONS.");
    return false;
  }

  if (!_rtspClient->SendDescribe()) {
    xmedia::log::WriteLog("xmedia", xmedia::log::Level::Err,
                          "failed to send rtsp DESCRIBE.");
    return false;
  }

  if (!_rtspClient->SendSetup()) {
    xmedia::log::WriteLog("xmedia", xmedia::log::Level::Err,
                          "failed to send rtsp SETUP.");
    return false;
  }

  if (!_rtspClient->SendPlay()) {
    xmedia::log::WriteLog("xmedia", xmedia::log::Level::Err,
                          "failed to send rtsp PLAY.");
    return false;
  }

  if (!_rtspClient->SendGetparameter()) {
    xmedia::log::WriteLog("xmedia", xmedia::log::Level::Err,
                          "failed to send rtsp GET_PARAMETER.");
    return false;
  }
  xmedia::log::WriteLog("xmedia", xmedia::log::Level::Info,
                        "rtsp stream is opened.");

  alive_ = true;

  return true;
}

void RTSPStream::CloseStream() {
  ////log::api::WriteLog(CORE_LOGGER, log::Level::Info, "Try to close RTSP
  /// stream..."); /log::api::WriteLog(CORE_LOGGER, log::Level::Info, "\t
  /// stream:
  ///[%s]", _rtspClient->RtspUrl.c_str());
  
  xmedia::log::WriteLog("xmedia", xmedia::log::Level::Info,
                        "[%d] try to close rtsp stream.",
                        std::this_thread::get_id());

  if (!_rtspClient) {
    xmedia::log::WriteLog("xmedia", xmedia::log::Level::Warn,
                          "no rtsp client");
  }
  if (!_rtspClient->SendTeardown()) {
    xmedia::log::WriteLog("xmedia", xmedia::log::Level::Warn,
                          "failed to send rtsp TEARDOWN.");
  }

  _rtspClient->ClearAllSession();

  Medium::close(_rtspClient);
  _rtspClient = nullptr;

  xmedia::log::WriteLog("xmedia", xmedia::log::Level::Info,
                        "rtsp stream is closed.");
}

CustomRTSPClient::CustomRTSPClient(UsageEnvironment& env, PacketQueue* queue,
                                   char const* rtspURL, int verbosityLevel,
                                   char const* applicationName,
                                   portNumBits tunnelOverHTTPPortNum,
                                   int socketNumToServer)
    : RTSPClient(env, rtspURL, verbosityLevel, applicationName,
                 tunnelOverHTTPPortNum, socketNumToServer),
      Auth(nullptr),
      Queue(queue),
      Session(nullptr),
      Subsession(nullptr),
      SubsessionIter(nullptr),
      ResetTask(nullptr),
      DescribeTask(nullptr),
      KeepAliveTask(nullptr),
      SupportGetParameter(false),
      UseTCP(false),
      NextDescribeDelay(1),
      RtspUrl(rtspURL),
      CallbackEvent(CreateEvent(NULL, false, false, NULL)),
      LastError(RTSPError::Unkown) {}

CustomRTSPClient::~CustomRTSPClient() {
  envir().taskScheduler().unscheduleDelayedTask(KeepAliveTask);

  if (Auth) {
    delete Auth;
    Auth = nullptr;
  }

  if (CallbackEvent) {
    CloseHandle(CallbackEvent);
  }

  Medium::close(Session);
  Session = nullptr;
  xmedia::log::WriteLog("xmedia", xmedia::log::Level::Info,
                        "[%d] rtsp client is destroyed.",
                        std::this_thread::get_id());
}

bool CustomRTSPClient::SendOptions() {
  if (!sendOptionsCommand(&CustomRTSPClient::OptionsCallback, Auth)) {
    xmedia::log::WriteLog("xmedia", xmedia::log::Level::Err,
                          "failed to send OPTIONS.");
    return false;
  }
  auto rt = WaitForSingleObject(CallbackEvent, SEND_TIMEOUT_MILLI);
  if (rt == WAIT_TIMEOUT) LastError = RTSPError::Timeout;
  return LastError == RTSPError::Success ? true : false;
}

bool CustomRTSPClient::SendDescribe() {
  if (!sendDescribeCommand(&CustomRTSPClient::DescribeCallback, Auth)) {
    xmedia::log::WriteLog("xmedia", xmedia::log::Level::Err,
                          "failed to send DESCRIBE.");
    return false;
  }
  auto rt = WaitForSingleObject(CallbackEvent, SEND_TIMEOUT_MILLI);
  if (rt == WAIT_TIMEOUT) LastError = RTSPError::Timeout;
  return LastError == RTSPError::Success ? true : false;
}

bool CustomRTSPClient::SendSetup() {
  while (auto subsession = SubsessionIter->next()) {
    if (subsession->initiate()) {
      Subsession = subsession;
      if (!sendSetupCommand(*subsession, SetupCallback, false, UseTCP, false,
                            Auth)) {
        xmedia::log::WriteLog("xmedia", xmedia::log::Level::Err,
                              "failed to send SEUPT.");
        return false;
      }
      // NOTE: Wait until each subsession is initialized.
      auto rt = WaitForSingleObject(CallbackEvent, SEND_TIMEOUT_MILLI);
      if (rt == WAIT_TIMEOUT) LastError = RTSPError::Timeout;
    }
  }

  return LastError == RTSPError::Success ? true : false;
}

bool CustomRTSPClient::SendPlay() {
  if (!sendPlayCommand(*Session, PlayCallback, -1.0f, -1.0f, 1.0f, Auth)) {
    xmedia::log::WriteLog("xmedia", xmedia::log::Level::Err,
                          "failed to send PLAY.");
    return false;
  }
  auto rt = WaitForSingleObject(CallbackEvent, SEND_TIMEOUT_MILLI);
  if (rt == WAIT_TIMEOUT) LastError = RTSPError::Timeout;
  return LastError == RTSPError::Success ? true : false;
}

bool CustomRTSPClient::SendTeardown() {
  if (!sendTeardownCommand(*Session, &CustomRTSPClient::TeardownCallback,
                           Auth)) {
    xmedia::log::WriteLog("xmedia", xmedia::log::Level::Err,
                          "failed to send TEARDOWN.");
    return false;
  }
  auto rt = WaitForSingleObject(CallbackEvent, SEND_TIMEOUT_MILLI);
  if (rt == WAIT_TIMEOUT) LastError = RTSPError::Timeout;
  return LastError == RTSPError::Success ? true : false;
}

bool CustomRTSPClient::SendGetparameter() {
  if (SupportGetParameter) {
    if (!sendGetParameterCommand(*Session, GetParameterCallback, "", Auth)) {
      xmedia::log::WriteLog("xmedia", xmedia::log::Level::Err,
                            "failed to send GET_PARAMETER.");
      return false;
    }
  }
  auto rt = WaitForSingleObject(CallbackEvent, SEND_TIMEOUT_MILLI);
  if (rt == WAIT_TIMEOUT) LastError = RTSPError::Timeout;
  return LastError == RTSPError::Success ? true : false;
}

void CustomRTSPClient::OptionsCallback(RTSPClient* rtspClient, int resultCode,
                                       char* resultString) {
  auto client = dynamic_cast<CustomRTSPClient*>(rtspClient);

  bool supportGetParameter = false;
  if (resultCode != 0) {
    xmedia::log::WriteLog("xmedia", xmedia::log::Level::Err,
                          "options callback error.[%d]", resultCode);
    client->LastError = RTSPError::Options;
  } else {
    if (strstr(resultString, "GET_PARAMETER") != NULL) {
      client->SupportGetParameter = true;
    } else {
      xmedia::log::WriteLog("xmedia", xmedia::log::Level::Info,
                            "not support GET_PARAMETER");
      ////log::api::WriteLog(CORE_LOGGER, log::Level::Info, "Not support
      /// GET_PARAMETER");
    }
    client->LastError = RTSPError::Success;
  }
  delete[] resultString;
  SetEvent(client->CallbackEvent);
}

void CustomRTSPClient::DescribeCallback(RTSPClient* rtspClient, int resultCode,
                                        char* resultString) {
  auto client = dynamic_cast<CustomRTSPClient*>(rtspClient);
  const char* sdp = nullptr;

  if (resultCode != 0) {
    xmedia::log::WriteLog("xmedia", xmedia::log::Level::Err,
                          "describe callback error.[%d]", resultCode);
    // client->scheduleDescribeCommand();
    client->LastError = RTSPError::Describe;
  } else {
    sdp = resultString;
    auto mediaSession = MediaSession::createNew(client->envir(), sdp);
    if (mediaSession == nullptr) {
      xmedia::log::WriteLog("xmedia", xmedia::log::Level::Err,
                            "failed to get media sessino from sdp.");
      ////log::api::WriteLog(CORE_LOGGER, log::Level::Err, "Failed to get media
      /// session from SDP");

      delete[] resultString;

      client->LastError = RTSPError::Describe;
      SetEvent(client->CallbackEvent);
      return;
    } else if (mediaSession->hasSubsessions() == False) {
      ////log::api::WriteLog(CORE_LOGGER, log::Level::Err, "Failed to get
      /// subsessions");
      Medium::close(mediaSession);

      delete[] resultString;

      client->LastError = RTSPError::Describe;
      SetEvent(client->CallbackEvent);
      return;
    }

    client->Session = mediaSession;
    client->SubsessionIter = new MediaSubsessionIterator(*mediaSession);

    // client->scheduleLivenessCommand();

    client->LastError = RTSPError::Success;
  }
  delete[] resultString;
  SetEvent(client->CallbackEvent);
}

void CustomRTSPClient::SetupCallback(RTSPClient* rtspClient, int resultCode,
                                     char* resultString) {
  auto client = dynamic_cast<CustomRTSPClient*>(rtspClient);

  if (resultCode != 0) {
    xmedia::log::WriteLog("xmedia", xmedia::log::Level::Err,
                          "setup callback error.[%d]", resultCode);
    client->LastError = RTSPError::Setup;
  } else {
    std::string videoStr = "video";
    std::string audioStr = "audio";
    const char* mediaName = client->Subsession->mediumName();

    MediaSink* mediaSink = nullptr;
    if (!videoStr.compare(mediaName)) {
      mediaSink = sink::SinkFactory::Create(
          client->envir(), client->Subsession->codecName(), client->Queue);
      client->Subsession->sink = mediaSink;

      mediaSink->startPlaying(*(client->Subsession->readSource()), nullptr,
                              nullptr);

    } else if (!audioStr.compare(mediaName)) {
      // Create audio sink
    } else {
      // not support error
    }
    client->LastError = RTSPError::Success;
  }
  delete[] resultString;
  SetEvent(client->CallbackEvent);
}

void CustomRTSPClient::PlayCallback(RTSPClient* rtspClient, int resultCode,
                                    char* resultString) {
  auto client = dynamic_cast<CustomRTSPClient*>(rtspClient);

  if (resultCode != 0) {
    xmedia::log::WriteLog("xmedia", xmedia::log::Level::Err,
                          "play callback error.[%d]", resultCode);
    client->LastError = RTSPError::Play;
  } else {
    client->LastError = RTSPError::Success;
  }
  delete[] resultString;
  SetEvent(client->CallbackEvent);
}

// Note: Don't check the result code as this callback is used in order to
// synchronize deleting the rtsp client only.
void CustomRTSPClient::TeardownCallback(RTSPClient* rtspClient, int resultCode,
                                        char* resultString) {
  auto client = dynamic_cast<CustomRTSPClient*>(rtspClient);

  if (resultCode != 0) {
    xmedia::log::WriteLog("xmedia", xmedia::log::Level::Err,
                          "teardown callback error.[%d]", resultCode);
  }
  delete[] resultString;
  client->LastError = RTSPError::Success;
  SetEvent(client->CallbackEvent);
}

void CustomRTSPClient::GetParameterCallback(RTSPClient* rtspClient,
                                            int resultCode,
                                            char* resultString) {
  auto client = dynamic_cast<CustomRTSPClient*>(rtspClient);

  if (resultCode != 0) {
    xmedia::log::WriteLog("xmedia", xmedia::log::Level::Err,
                          "getparameter callback error.[%d]", resultCode);
    if (resultCode < 0) {
      xmedia::log::WriteLog("xmedia", xmedia::log::Level::Err,
                            "rtsp connection is lost.");
    }
    delete[] resultString;
    return;
  }
  delete[] resultString;

  client->ScheduleLivenessCommand();
  client->LastError = RTSPError::Success;
  SetEvent(client->CallbackEvent);
}

void CustomRTSPClient::ScheduleDescribeCommand() {
  unsigned delayMax =
      sessionTimeoutParameter();  // if the server specified a maximum time
                                  // between 'liveness' probes, then use that
  if (delayMax == 0) {
    delayMax = 60;
  }

  // Choose a random time from [delayMax/2,delayMax-1) seconds:
  unsigned const us_1stPart = delayMax * 500000;
  unsigned uSecondsToDelay;
  if (us_1stPart <= 1000000) {
    uSecondsToDelay = us_1stPart;
  } else {
    unsigned const us_2ndPart = us_1stPart - 1000000;
    uSecondsToDelay = us_1stPart + (us_2ndPart * our_random()) % us_2ndPart;
  }

  DescribeTask = envir().taskScheduler().scheduleDelayedTask(
      uSecondsToDelay, ScheduleDescribe, this);
}

void CustomRTSPClient::ScheduleLivenessCommand() {
  unsigned delayMax =
      sessionTimeoutParameter();  // if the server specified a maximum time
                                  // between 'liveness' probes, then use that
  if (delayMax == 0) {
    delayMax = 60;
  }

  // Choose a random time from [delayMax/2,delayMax-1) seconds:
  unsigned const us_1stPart = delayMax * 500000;
  unsigned uSecondsToDelay;
  if (us_1stPart <= 1000000) {
    uSecondsToDelay = us_1stPart;
  } else {
    unsigned const us_2ndPart = us_1stPart - 1000000;
    uSecondsToDelay = us_1stPart + (us_2ndPart * our_random()) % us_2ndPart;
  }

  KeepAliveTask = envir().taskScheduler().scheduleDelayedTask(
      uSecondsToDelay, ScheduleLiveness, this);
}

void CustomRTSPClient::ScheduleDescribe(void* clientData) {
  auto client = (CustomRTSPClient*)clientData;

  if (client) {
    client->DescribeTask = NULL;
    client->SendDescribe();
  }
}

void CustomRTSPClient::ScheduleLiveness(void* clientData) {
  CustomRTSPClient* rtspClient = (CustomRTSPClient*)clientData;
  rtspClient->KeepAliveTask = NULL;

  MediaSession* sess = rtspClient->Session;

  if (rtspClient->SupportGetParameter && sess != NULL) {
    rtspClient->sendGetParameterCommand(*sess, GetParameterCallback, "",
                                        rtspClient->Auth);
  } else {
    rtspClient->sendOptionsCommand(OptionsCallback, rtspClient->Auth);
  }
}
// void CustomRTSPClient::continueAfterLivenessCommand(int resultCode, bool
// supportGetParameter)
//{
//    if (resultCode != 0) {
//        SupportGetParameter = false;
//        if (resultCode < 0) {
//            fprintf(stderr, "lost connection....\n");
//        }
//        scheduleReset();
//        return;
//    }
//    SupportGetParameter = supportGetParameter;
//
//    scheduleLivenessCommand();
//}

void CustomRTSPClient::scheduleReset() {
  envir().taskScheduler().rescheduleDelayedTask(ResetTask, 0, DoReset, this);
}

// void CustomRTSPClient::SendGetParameter(void* param)
//{
//    auto client = (CustomRTSPClient*)param;
//    client->KeepAliveTask = NULL;
//
//    MediaSession* sess = client->Session;
//    client->sendGetParameterCommand(*sess, GetParameterCallback, "");
//}

// void CustomRTSPClient::SendDescribe()
//{
//    sendDescribeCommand(DescribeCallback, Auth);
//}

void CustomRTSPClient::ClearAllSession() {
  auto session = Session;
  if (session != nullptr) {
    MediaSubsessionIterator iter(*session);

    while (auto subsession = iter.next()) {
      if (subsession->sink) {
        Medium::close(subsession->sink);
        subsession->sink = nullptr;

        if (subsession->rtcpInstance()) {
          subsession->rtcpInstance()->setByeHandler(NULL, NULL);
        }
      }
    }
  }

  if (SubsessionIter) {
    delete SubsessionIter;
    SubsessionIter = nullptr;
  }

  /*Medium::close(Session);
  Session = nullptr;*/
  xmedia::log::WriteLog("xmedia", xmedia::log::Level::Info,
                        "all sessions are cleaned.");
}

void CustomRTSPClient::Reset() {
  envir().taskScheduler().unscheduleDelayedTask(KeepAliveTask);
  envir().taskScheduler().unscheduleDelayedTask(DescribeTask);
  envir().taskScheduler().unscheduleDelayedTask(ResetTask);

  RTSPClient::reset();
}

void CustomRTSPClient::DoReset(void* clientData) {
  auto client = (CustomRTSPClient*)clientData;
  client->DoReset();
}

void CustomRTSPClient::DoReset() {
  ResetTask = NULL;

  Reset();

  ClearAllSession();

  setBaseURL(RtspUrl.c_str());
  SendDescribe();
}

}  // namespace core
}  // namespace xmedia