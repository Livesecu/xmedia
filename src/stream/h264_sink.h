#pragma once

#include "decoder/core_commons.h"
#include "decoder/packet_queue.h"

namespace xmedia {
namespace core {
namespace sink {

class H264Sink : public MediaSink {
 public:
  static H264Sink* Create(UsageEnvironment& env, PacketQueue* queue);

 protected:
  H264Sink(UsageEnvironment& env, PacketQueue* queue);
  virtual ~H264Sink();

 private:
  static void afterGettingFrame(void* clientData, unsigned frameSize,
                                unsigned numTruncatedBytes,
                                struct timeval presentationTime,
                                unsigned durationInMicroseconds);

  void afterGettingFrame(unsigned frameSize, unsigned numTruncatedBytes,
                         struct timeval presentationTime,
                         unsigned durationInMicroseconds);

  virtual Boolean continuePlaying() override;

  uint8_t* _buffer;
  PacketQueue* _queue;

#ifdef USE_CONSOLE_DEBUG
  int id;
#endif
};

}  // namespace sink
}  // namespace core
}  // namespace xmedia