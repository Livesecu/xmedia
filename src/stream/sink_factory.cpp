#include "sink_factory.h"

#include <string>

#include "h264_sink.h"

namespace xmedia {
namespace core {
namespace sink {

MediaSink* SinkFactory::Create(UsageEnvironment& env, const char* codecName,
                               PacketQueue* queue) {
  if (!strcmp(codecName, "H264") || !strcmp(codecName, "h264")) {
    return H264Sink::Create(env, queue);
  }

  return nullptr;
}

}  // namespace sink
}  // namespace core
}  // namespace xmedia