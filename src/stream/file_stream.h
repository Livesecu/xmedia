#pragma once

#include "decoder/core_commons.h"
#include "decoder/demuxer.h"


namespace livemediacore{

class FileStreamReader
{
protected:
    std::string _path;

public:
    FileStreamReader(const char* path);
    virtual ~FileStreamReader();


    virtual bool OpenStream(const char* path);

};


//class FileStreamWriter
//{
//
//};

}   // livemediacore