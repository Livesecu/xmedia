#include "h264_sink.h"
#include "log/logger.h"

#ifdef USE_CONSOLE_DEBUG
#include <random>
#endif

namespace xmedia {
namespace core {
namespace sink {

H264Sink* H264Sink::Create(UsageEnvironment& env, PacketQueue* queue) {
  return new H264Sink(env, queue);
}

H264Sink::H264Sink(UsageEnvironment& env, PacketQueue* queue)
    : MediaSink(env), _buffer(new uint8_t[RECV_BUFFER_SIZE]), _queue(queue) {
#ifdef USE_CONSOLE_DEBUG
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<int> dis(0, 10);

  id = dis(gen);
#endif
  xmedia::log::WriteLog("xmedia", xmedia::log::Level::Debug,
                        "[%d] h264 sink is created.",
                        std::this_thread::get_id());
}

H264Sink::~H264Sink() {
  stopPlaying();

  if (_queue) {
    auto packet = new Packet(1);
    packet->abortFlag = 1;
    _queue->Put(packet);
    xmedia::log::WriteLog("xmedia", xmedia::log::Level::Debug,
                          "send abort packet.");
  }
  if (_buffer) {
    delete[] _buffer;
    _buffer = nullptr;
  }
  xmedia::log::WriteLog("xmedia", xmedia::log::Level::Debug,
                        "h264 sink is destroyed.");
}

void H264Sink::afterGettingFrame(void* clientData, unsigned frameSize,
                                 unsigned numTruncatedBytes,
                                 struct timeval presentationTime,
                                 unsigned durationInMicroseconds) {
  auto sink = (H264Sink*)clientData;
  sink->afterGettingFrame(frameSize, numTruncatedBytes, presentationTime,
                          durationInMicroseconds);
}

void H264Sink::afterGettingFrame(unsigned frameSize, unsigned numTruncatedBytes,
                                 struct timeval presentationTime,
                                 unsigned durationInMicroseconds) {
#ifdef USE_CONSOLE_DEBUG
  const char byteFormat[] = "%02x %02x %02x %02x";
  char byteArray[sizeof byteFormat];

  sprintf(byteArray, byteFormat, _buffer[0], _buffer[1], _buffer[2],
          _buffer[3]);

  fprintf(stderr, "(%d) Received Frame [%ld] (%s)\n", id, frameSize, byteArray);
#endif

  if (_queue && _buffer) {
    auto packet = new Packet(frameSize + 3);

    packet->mediaType = MediaType::Video;
    strcpy(packet->codecName, "H264");
    packet->pts =
        presentationTime.tv_sec * 1000000LL + presentationTime.tv_usec;
    packet->data[0] = 0x00;
    packet->data[1] = 0x00;
    packet->data[2] = 0x01;
    packet->size = frameSize + 3;
    memcpy_s(&packet->data[3], frameSize, _buffer, frameSize);

    _queue->Put(packet);
  }
  // Then continue, to request the next frame of data:
  continuePlaying();
}

Boolean H264Sink::continuePlaying() {
  if (fSource == NULL) return False;

  // Request the next frame of data from our input source. "afterGettingFrame()"
  // will get called later, when it arrives:
  fSource->getNextFrame(_buffer, RECV_BUFFER_SIZE, afterGettingFrame, this,
                        onSourceClosure, this);
  return True;
}

}  // namespace sink
}  // namespace core
}  // namespace xmedia