#include <iostream>
#include <functional>

#include "LiveMediaRender.h"

Renderer* render = nullptr;

LRESULT WINAPI WindowProcedure(HWND window, unsigned int msg, WPARAM wp, LPARAM lp)
{
    switch (msg) {
    case WM_DESTROY:
#ifdef USE_CONSOLE_DEBUG
        std::cout << "\ndestroying window\n";
#endif
        //ClearStreams(render);
        PostQuitMessage(0);
        return 0L;
    case WM_LBUTTONDOWN:
#ifdef USE_CONSOLE_DEBUG
        std::cout << "\nmouse left button down at (" << LOWORD(lp)
            << ',' << HIWORD(lp) << ")\n";
#endif
        ClearStreams(render);
        break;
    case WM_RBUTTONDOWN:
#ifdef USE_CONSOLE_DEBUG
        std::cout << "\nmouse right button down at (" << LOWORD(lp)
            << ',' << HIWORD(lp) << ")\n";
#endif
        break;
    default:
        return DefWindowProc(window, msg, wp, lp);
    }
}

struct SimpleWindow
{
    HWND handle;

    SimpleWindow(int width, int height)
    {
        const char* myclass = "BaseWindow";

        WNDCLASSEX wndclass = {
            sizeof(WNDCLASSEX),
            CS_DBLCLKS,
            WindowProcedure,
            0, 0,
            GetModuleHandle(0),
            LoadIcon(0,IDI_APPLICATION),
            LoadCursor(0,IDC_ARROW),
            HBRUSH(COLOR_WINDOW + 1),
            0,
            myclass,
            LoadIcon(0,IDI_APPLICATION) };

        if (RegisterClassEx(&wndclass)) {
            handle = CreateWindowEx(0, myclass, "RenderWindow",
                                           WS_OVERLAPPEDWINDOW,
                                           CW_USEDEFAULT, CW_USEDEFAULT,
                                           width, height,
                                           0,
                                           0,
                                           GetModuleHandle(0),
                                           0);
        }
    }

    virtual ~SimpleWindow()
    {

    }

    void EventLoop()
    {
        if (handle) {
            ShowWindow(handle, SW_SHOWDEFAULT);
            MSG msg;
            while (true) {
                while (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE)) {
                    DispatchMessage(&msg);
                }
                if (render) {
                    Display(render);
                    Sleep(30);
                }
            }
        }
    }
};

int main()
{
    fprintf(stderr, "hello world\n");
    auto window = new SimpleWindow(1280, 720);
    render = CreateRender(window->handle, 1280, 720);

    if (EnsureDevice(render) == false) {
        std::cout << "Failed to ensure the render device" << std::endl;
        exit(1);
    }

    const char* url1 = "rtsp://192.168.0.195:554/main";
    const char* user = "admin";
    const char* password = "1q2w3e4r";
    OpenStream(render, url1, user, password, (int)(10), (int)(10), 400, 300);
    /*render->OpenStream(url1, user, password, (int)(0), (int)(0), 400, 300);
    render->OpenStream(url1, user, password, (int)(0), (int)(0), 400, 300);
    render->OpenStream(url1, user, password, (int)(0), (int)(0), 400, 300);
    render->OpenStream(url1, user, password, (int)(0), (int)(0), 400, 300);
    render->OpenStream(url1, user, password, (int)(0), (int)(0), 400, 300);
    render->OpenStream(url1, user, password, (int)(0), (int)(0), 400, 300);
    render->OpenStream(url1, user, password, (int)(0), (int)(0), 400, 300);*/

    const char* url2 = "rtsp://192.168.0.194:554/main";
    OpenStream(render, url2, user, password, (int)(410), (int)(310), 400, 300);
    /*render->OpenStream(url2, user, password, (int)(400), (int)(300), 400, 300);
    render->OpenStream(url2, user, password, (int)(400), (int)(300), 400, 300);
    render->OpenStream(url2, user, password, (int)(400), (int)(300), 400, 300);
    render->OpenStream(url2, user, password, (int)(400), (int)(300), 400, 300);
    render->OpenStream(url2, user, password, (int)(400), (int)(300), 400, 300);
    render->OpenStream(url2, user, password, (int)(400), (int)(300), 400, 300);
    render->OpenStream(url2, user, password, (int)(400), (int)(300), 400, 300);
    render->OpenStream(url2, user, password, (int)(400), (int)(300), 400, 300);
    render->OpenStream(url2, user, password, (int)(400), (int)(300), 400, 300);*/


    //Sleep(10000);

    //DisplayFunc = std::bind(&Renderer::Display, render);

    window->EventLoop();

    return 0;
}