#include <iostream>
#include <array>

#include "dll/xmedia.h"

int main(int argc, char* argv[]) {
  std::cout << "Start testing rtsp stream..." << std::endl;

  constexpr std::array urls{"rtsp://192.168.0.190/main",
                            "rtsp://192.168.0.197/main"};
  constexpr std::array users{"admin", "admin"};
  constexpr std::array pwds{"123456", "tta007!@#"};

  // 스트림 열기/닫기는 동일한 쓰레드에서만 사용해야함!!
  for (size_t i = 0; i < 100; ++i) {
    void* stream1 = nullptr;
    void* stream2 = nullptr;
    TestOpenStream(urls[0], users[0], pwds[0], &stream1);
    TestOpenStream(urls[1], users[1], pwds[1], &stream2);

    Sleep(5 * 1000);

    TestCloseStream(stream1);
    TestCloseStream(stream2);
  }

  std::cout << std::endl;
  std::cout << "End testing rtsp stream..." << std::endl;

  return 0;
}