﻿using System;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Threading;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace test_render
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DispatcherTimer sizeTimer;
        DispatcherTimer adapterTimer;
        TimeSpan lastRender;

        public MainWindow()
        {
            InitializeComponent();

            HRESULT.Check(SetSize(512, 512));
            HRESULT.Check(SetAlpha(false));
            HRESULT.Check(SetNumDesiredSamples(4));

            CompositionTarget.Rendering += new EventHandler(CompositionTargetRendering);

            adapterTimer = new DispatcherTimer();
            adapterTimer.Tick += new EventHandler(AdapterTimerTick);
            adapterTimer.Interval = new TimeSpan(0, 0, 0, 0, 500);
            adapterTimer.Start();

            sizeTimer = new DispatcherTimer(DispatcherPriority.Render);
            sizeTimer.Tick += new EventHandler(SizeTimerTick);
            sizeTimer.Interval = new TimeSpan(0, 0, 0, 0, 200);
            sizeTimer.Start();
        }

        ~MainWindow()
        {
            Destroy();
        }

        void AdapterTimerTick(object sender, EventArgs e)
        {
            POINT p = new POINT(imgelt.PointToScreen(new Point(0, 0)));
            HRESULT.Check(SetAdapter(p));
        }

        void SizeTimerTick(object sender, EventArgs e)
        {
            uint actualWidth = (uint)imgelt.ActualWidth;
            uint actualHeight = (uint)imgelt.ActualHeight;
            if ((actualWidth > 0 && actualHeight > 0) &&
                    (actualWidth != (uint)d3dimg.Width || actualHeight != (uint)d3dimg.Height))
            {
                HRESULT.Check(SetSize(actualWidth, actualHeight));
            }
        }

        void CompositionTargetRendering(object sender, EventArgs e)
        {
            RenderingEventArgs args = (RenderingEventArgs)e;

            if (d3dimg.IsFrontBufferAvailable && lastRender != args.RenderingTime)
            {
                IntPtr surface = IntPtr.Zero;
                HRESULT.Check(GetBackBufferNoRef(out surface));
                if (surface != IntPtr.Zero)
                {
                    d3dimg.Lock();
                    d3dimg.SetBackBuffer(D3DResourceType.IDirect3DSurface9, surface);
                    HRESULT.Check(Render());
                    d3dimg.AddDirtyRect(new Int32Rect(0, 0, d3dimg.PixelWidth, d3dimg.PixelHeight));
                    d3dimg.Unlock();

                    lastRender = args.RenderingTime;
                }
            }
        }

        [DllImport("xmedia.dll")]
        static extern int GetBackBufferNoRef(out IntPtr pSurface);

        [DllImport("xmedia.dll")]
        static extern int SetSize(uint width, uint height);

        [DllImport("xmedia.dll")]
        static extern int SetAlpha(bool useAlpha);

        [DllImport("xmedia.dll")]
        static extern int SetNumDesiredSamples(uint numSamples);

        [StructLayout(LayoutKind.Sequential)]
        struct POINT
        {
            public POINT(Point p)
            {
                x = (int)p.X;
                y = (int)p.Y;
            }
            public int x;
            public int y;
        }

        [DllImport("xmedia.dll")]
        static extern int SetAdapter(POINT screenSpacePoint);

        [DllImport("xmedia.dll")]
        static extern int Render();

        [DllImport("xmedia.dll")]
        static extern void Destroy();

        [DllImport("xmedia.dll")]
        static extern void StreamTest();
    }

    public static class HRESULT
    {
        [SecurityPermissionAttribute(SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode)]
        public static void Check(int hr)
        {
            Marshal.ThrowExceptionForHR(hr);
        }
    }
}
