#include "dll/xmedia.h"

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include <iostream>
#include <thread>

int boxid = 0;

LRESULT WINAPI WindowProcedure(HWND window, unsigned int msg, WPARAM wp,
                               LPARAM lp);

struct SimpleWindow {
  HWND handle;

  SimpleWindow(int width, int height) : handle(nullptr) {
    const wchar_t* myclass = L"Test Window";

    WNDCLASSEX wndclass = {sizeof(WNDCLASSEX),
                           CS_DBLCLKS,
                           WindowProcedure,
                           0,
                           0,
                           GetModuleHandle(0),
                           LoadIcon(0, IDI_APPLICATION),
                           LoadCursor(0, IDC_ARROW),
                           HBRUSH(COLOR_WINDOW + 1),
                           0,
                           myclass,
                           LoadIcon(0, IDI_APPLICATION)};

    if (RegisterClassEx(&wndclass)) {
      handle = CreateWindowEx(0, myclass, myclass, WS_OVERLAPPEDWINDOW,
                              CW_USEDEFAULT, CW_USEDEFAULT, width, height, 0, 0,
                              GetModuleHandle(0), 0);
    }
  }

  void EventLoop() {
    if (handle) {
      ShowWindow(handle, SW_SHOWDEFAULT);
      MSG msg;
      while (true) {
        while (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE)) {
          DispatchMessage(&msg);
        }
        Present(handle);
        Sleep(40);
      }
    }
  }
};

LRESULT WINAPI WindowProcedure(HWND window, unsigned int msg, WPARAM wp,
                               LPARAM lp) {
  switch (msg) {
    case WM_DESTROY:
      std::cout << "\ndestroying window\n";
      RemoveViewSource(boxid);
      RemoveSubView(boxid);
      PostQuitMessage(0);
      return 0L;
    case WM_LBUTTONDOWN:
      std::cout << "\nmouse left button down at (" << LOWORD(lp) << ','
                << HIWORD(lp) << ")\n";
      break;
    case WM_RBUTTONDOWN:
      std::cout << "\nmouse right button down at (" << LOWORD(lp) << ','
                << HIWORD(lp) << ")\n";
      break;
    default:
      return DefWindowProc(window, msg, wp, lp);
  }
}

int main() {
  LoadXmediaPlayer("D:\\xmedia_log\\");

  CreateViewControl(nullptr);
  /*if (viewControl == nullptr) {
      fprintf(stderr, "Failed to create the view control\n");
      return 1;
  }*/

  if (InitializeViewControl() < 0) {
    fprintf(stderr, "Failed to initialize view control\n");
    return 1;
  }

  SimpleWindow testWindow(1280, 720);

  RECT rect;
  ::GetClientRect(testWindow.handle, &rect);

  int width = rect.left + rect.right;
  int height = rect.top + rect.bottom;

  // livemediaplayer::CreateMainView(viewControl, (double)rect.left,
  // (double)rect.top, (double)width, (double)height);
  boxid = CreateSubView((double)rect.left, (double)rect.top, (double)width,
                        (double)height, 0.0);

  AddViewSource(boxid, "rtsp://192.168.0.195/main", "admin", "1q2w3e4r", false);
  // xmedia::player::api::AddViewSource(viewControl, boxid,
  // "rtsp://admin:!qaz2wsx3edc@hscamera.iptime.org", "", "", false);
  // livemediaplayer::AddViewSource(viewControl, boxid,
  // "rtsp://hscamera.iptime.org/main", "admin", "!qaz2wsx3edc");

  testWindow.EventLoop();

  return 0;
}