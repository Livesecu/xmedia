﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Interop;
using System.Runtime.InteropServices;
using System.Windows.Controls;

namespace win_player_wpf
{
    public partial class MainWindow : Window
    {
        private IntPtr _viewControl = IntPtr.Zero;
        private IntPtr _scene = IntPtr.Zero;
        private D3DImage _di = null;

        private TimeSpan _lastRenderTime;

        public MainWindow()
        {
            InitializeComponent();
            LoadXmediaPlayer("D:\\xmedia_log\\");
            this.Loaded += OnLoaded;
        }

        void OnLoaded(object sender, RoutedEventArgs e)
        {
            CreateViewControl((PresentationSource.FromVisual(this) as HwndSource).Handle);
            if(InitializeViewControl() < 0)
            {
                MessageBox.Show("Failed to initialize view control.");
                return;
            };

            _di = new D3DImage();
            _di.IsFrontBufferAvailableChanged += OnIsFrontBufferAvailableChanged;

            BeginRenderingScene();

            /// view 1 ////
            Point gridPoint1 = live1_grid.TransformToAncestor(Application.Current.MainWindow).Transform(new Point(0, 0));
            live1_grid.Tag = CreateSubView( gridPoint1.X, gridPoint1.Y, live1_grid.ActualWidth, live1_grid.ActualHeight, 0.0);

            Canvas newCanvas1 = new Canvas();

            Rect rect1;
            rect1.X = gridPoint1.X;
            rect1.Y = gridPoint1.Y;
            rect1.Width = live1_grid.ActualWidth;
            rect1.Height = live1_grid.ActualHeight;

            var rectangle1 = new Rectangle();
            rectangle1.Width = rect1.Width;
            rectangle1.Height = rect1.Height;

            var brush1 = new ImageBrush(_di)
            {
                Viewbox = rect1,
                ViewboxUnits = BrushMappingMode.Absolute,
            };

            rectangle1.Fill = brush1;
            newCanvas1.Children.Add(rectangle1);
            live1_grid.Children.Add(newCanvas1);

            ///// view 2 ////
            Point gridPoint2 = live2_grid.TransformToAncestor(Application.Current.MainWindow).Transform(new Point(0, 0));
            live2_grid.Tag = CreateSubView( gridPoint2.X, gridPoint2.Y, live2_grid.ActualWidth, live2_grid.ActualHeight, 0.0);

            Rect rect2;
            rect2.X = gridPoint2.X;
            rect2.Y = gridPoint2.Y;
            rect2.Width = live2_grid.ActualWidth;
            rect2.Height = live2_grid.ActualHeight;

            var rectangle2 = new Rectangle();
            rectangle2.Width = rect2.Width;
            rectangle2.Height = rect2.Height;

            var brush2 = new ImageBrush(_di)
            {
                Viewbox = rect2,
                ViewboxUnits = BrushMappingMode.Absolute,
            };

            rectangle2.Fill = brush2;
            canvas2.Children.Add(rectangle2);

            ///// view 3 ////
            Point gridPoint3 = live3_grid.TransformToAncestor(Application.Current.MainWindow).Transform(new Point(0, 0));
            live3_grid.Tag = CreateSubView( gridPoint3.X, gridPoint3.Y, live3_grid.ActualWidth, live3_grid.ActualHeight, 0.0);

            Rect rect3;
            rect3.X = gridPoint3.X;
            rect3.Y = gridPoint3.Y;
            rect3.Width = live3_grid.ActualWidth;
            rect3.Height = live3_grid.ActualHeight;

            var rectangle3 = new Rectangle();
            rectangle3.Width = rect3.Width;
            rectangle3.Height = rect3.Height;

            var brush3 = new ImageBrush(_di)
            {
                Viewbox = rect3,
                ViewboxUnits = BrushMappingMode.Absolute,
            };

            rectangle3.Fill = brush3;
            canvas3.Children.Add(rectangle3);

            ///// view 4 ////
            Point gridPoint4 = live4_grid.TransformToAncestor(Application.Current.MainWindow).Transform(new Point(0, 0));
            live4_grid.Tag = CreateSubView( gridPoint4.X, gridPoint4.Y, live4_grid.ActualWidth, live4_grid.ActualHeight, 0.0);

            Rect rect4;
            rect4.X = gridPoint4.X;
            rect4.Y = gridPoint4.Y;
            rect4.Width = live4_grid.ActualWidth;
            rect4.Height = live4_grid.ActualHeight;

            var rectangle4 = new Rectangle();
            rectangle4.Width = rect4.Width;
            rectangle4.Height = rect4.Height;

            var brush4 = new ImageBrush(_di)
            {
                Viewbox = rect4,
                ViewboxUnits = BrushMappingMode.Absolute,
            };

            rectangle4.Fill = brush4;
            canvas4.Children.Add(rectangle4);

            ///// view 5 ////
            Point gridPoint5 = live5_grid.TransformToAncestor(Application.Current.MainWindow).Transform(new Point(0, 0));
            live5_grid.Tag = CreateSubView( gridPoint5.X, gridPoint5.Y, live5_grid.ActualWidth, live5_grid.ActualHeight, 0.0);

            Rect rect5;
            rect5.X = gridPoint5.X;
            rect5.Y = gridPoint5.Y;
            rect5.Width = live5_grid.ActualWidth;
            rect5.Height = live5_grid.ActualHeight;

            var rectangle5 = new Rectangle();
            rectangle5.Width = rect5.Width;
            rectangle5.Height = rect5.Height;

            var brush5 = new ImageBrush(_di)
            {
                Viewbox = rect5,
                ViewboxUnits = BrushMappingMode.Absolute,
            };

            rectangle5.Fill = brush5;
            canvas5.Children.Add(rectangle5);

            ///// view 6 ////
            Point gridPoint6 = live6_grid.TransformToAncestor(Application.Current.MainWindow).Transform(new Point(0, 0));
            live6_grid.Tag = CreateSubView( gridPoint6.X, gridPoint6.Y, live6_grid.ActualWidth, live6_grid.ActualHeight, 0.0);

            Rect rect6;
            rect6.X = gridPoint6.X;
            rect6.Y = gridPoint6.Y;
            rect6.Width = live6_grid.ActualWidth;
            rect6.Height = live6_grid.ActualHeight;

            var rectangle6 = new Rectangle();
            rectangle6.Width = rect6.Width;
            rectangle6.Height = rect6.Height;

            var brush6 = new ImageBrush(_di)
            {
                Viewbox = rect6,
                ViewboxUnits = BrushMappingMode.Absolute,
            };

            rectangle6.Fill = brush6;
            canvas6.Children.Add(rectangle6);

            ///// view 7 ////
            Point gridPoint7 = live7_grid.TransformToAncestor(Application.Current.MainWindow).Transform(new Point(0, 0));
            live7_grid.Tag = CreateSubView( gridPoint7.X, gridPoint7.Y, live7_grid.ActualWidth, live7_grid.ActualHeight, 0.0);

            Rect rect7;
            rect7.X = gridPoint7.X;
            rect7.Y = gridPoint7.Y;
            rect7.Width = live7_grid.ActualWidth;
            rect7.Height = live7_grid.ActualHeight;

            var rectangle7 = new Rectangle();
            rectangle7.Width = rect7.Width;
            rectangle7.Height = rect7.Height;

            var brush7 = new ImageBrush(_di)
            {
                Viewbox = rect7,
                ViewboxUnits = BrushMappingMode.Absolute,
            };

            rectangle7.Fill = brush7;
            canvas7.Children.Add(rectangle7);

            ///// view 8 ////
            Point gridPoint8 = live8_grid.TransformToAncestor(Application.Current.MainWindow).Transform(new Point(0, 0));
            live8_grid.Tag = CreateSubView( gridPoint8.X, gridPoint8.Y, live8_grid.ActualWidth, live8_grid.ActualHeight, 0.0);

            Rect rect8;
            rect8.X = gridPoint8.X;
            rect8.Y = gridPoint8.Y;
            rect8.Width = live8_grid.ActualWidth;
            rect8.Height = live8_grid.ActualHeight;

            var rectangle8 = new Rectangle();
            rectangle8.Width = rect8.Width;
            rectangle8.Height = rect8.Height;

            var brush8 = new ImageBrush(_di)
            {
                Viewbox = rect8,
                ViewboxUnits = BrushMappingMode.Absolute,
            };

            rectangle8.Fill = brush8;
            canvas8.Children.Add(rectangle8);

            ///// view 9 ////
            Point gridPoint9 = live9_grid.TransformToAncestor(Application.Current.MainWindow).Transform(new Point(0, 0));
            live9_grid.Tag = CreateSubView( gridPoint9.X, gridPoint9.Y, live9_grid.ActualWidth, live9_grid.ActualHeight, 0.0);

            Rect rect9;
            rect9.X = gridPoint9.X;
            rect9.Y = gridPoint9.Y;
            rect9.Width = live9_grid.ActualWidth;
            rect9.Height = live9_grid.ActualHeight;

            var rectangle9 = new Rectangle();
            rectangle9.Width = rect9.Width;
            rectangle9.Height = rect9.Height;

            var brush9 = new ImageBrush(_di)
            {
                Viewbox = rect9,
                ViewboxUnits = BrushMappingMode.Absolute,
            };

            rectangle9.Fill = brush9;
            canvas9.Children.Add(rectangle9);

            ///// view 10 ////
            Point gridPoint10 = live10_grid.TransformToAncestor(Application.Current.MainWindow).Transform(new Point(0, 0));
            live10_grid.Tag = CreateSubView( gridPoint10.X, gridPoint10.Y, live10_grid.ActualWidth, live10_grid.ActualHeight, 0.0);

            Rect rect10;
            rect10.X = gridPoint10.X;
            rect10.Y = gridPoint10.Y;
            rect10.Width = live10_grid.ActualWidth;
            rect10.Height = live10_grid.ActualHeight;

            var rectangle10 = new Rectangle();
            rectangle10.Width = rect10.Width;
            rectangle10.Height = rect10.Height;

            var brush10 = new ImageBrush(_di)
            {
                Viewbox = rect10,
                ViewboxUnits = BrushMappingMode.Absolute,
            };

            rectangle10.Fill = brush10;
            canvas10.Children.Add(rectangle10);

            ///// view 11 ////
            Point gridPoint11 = live11_grid.TransformToAncestor(Application.Current.MainWindow).Transform(new Point(0, 0));
            live11_grid.Tag = CreateSubView( gridPoint11.X, gridPoint11.Y, live11_grid.ActualWidth, live11_grid.ActualHeight, 0.0);

            Rect rect11;
            rect11.X = gridPoint11.X;
            rect11.Y = gridPoint11.Y;
            rect11.Width = live11_grid.ActualWidth;
            rect11.Height = live11_grid.ActualHeight;

            var rectnagle11 = new Rectangle();
            rectnagle11.Width = rect11.Width;
            rectnagle11.Height = rect11.Height;

            var brush11 = new ImageBrush(_di)
            {
                Viewbox = rect11,
                ViewboxUnits = BrushMappingMode.Absolute,
            };

            rectnagle11.Fill = brush11;
            canvas11.Children.Add(rectnagle11);

            ///// view 12 ////
            Point gridPoint12 = live12_grid.TransformToAncestor(Application.Current.MainWindow).Transform(new Point(0, 0));
            live12_grid.Tag = CreateSubView( gridPoint12.X, gridPoint12.Y, live12_grid.ActualWidth, live12_grid.ActualHeight, 0.0);

            Rect rect12;
            rect12.X = gridPoint12.X;
            rect12.Y = gridPoint12.Y;
            rect12.Width = live12_grid.ActualWidth;
            rect12.Height = live12_grid.ActualHeight;

            var ractangle12 = new Rectangle();
            ractangle12.Width = rect12.Width;
            ractangle12.Height = rect12.Height;

            var brush12 = new ImageBrush(_di)
            {
                Viewbox = rect12,
                ViewboxUnits = BrushMappingMode.Absolute,
            };

            ractangle12.Fill = brush12;
            canvas12.Children.Add(ractangle12);

            ///// view 13 ////
            Point gridPoint13 = live13_grid.TransformToAncestor(Application.Current.MainWindow).Transform(new Point(0, 0));
            live13_grid.Tag = CreateSubView( gridPoint13.X, gridPoint13.Y, live13_grid.ActualWidth, live13_grid.ActualHeight, 0.0);

            Rect rect13;
            rect13.X = gridPoint13.X;
            rect13.Y = gridPoint13.Y;
            rect13.Width = live13_grid.ActualWidth;
            rect13.Height = live13_grid.ActualHeight;

            var ractangle13 = new Rectangle();
            ractangle13.Width = rect13.Width;
            ractangle13.Height = rect13.Height;

            var brush13 = new ImageBrush(_di)
            {
                Viewbox = rect13,
                ViewboxUnits = BrushMappingMode.Absolute,
            };

            ractangle13.Fill = brush13;
            canvas13.Children.Add(ractangle13);

            ///// view 14 ////
            Point gridPoint14 = live14_grid.TransformToAncestor(Application.Current.MainWindow).Transform(new Point(0, 0));
            live14_grid.Tag = CreateSubView( gridPoint14.X, gridPoint14.Y, live14_grid.ActualWidth, live14_grid.ActualHeight, 0.0);

            Rect rect14;
            rect14.X = gridPoint14.X;
            rect14.Y = gridPoint14.Y;
            rect14.Width = live14_grid.ActualWidth;
            rect14.Height = live14_grid.ActualHeight;

            var rectangle14 = new Rectangle();
            rectangle14.Width = rect14.Width;
            rectangle14.Height = rect14.Height;

            var brush14 = new ImageBrush(_di)
            {
                Viewbox = rect14,
                ViewboxUnits = BrushMappingMode.Absolute,
            };

            rectangle14.Fill = brush14;
            canvas14.Children.Add(rectangle14);

            ///// view 15 ////
            Point gridPoint15 = live15_grid.TransformToAncestor(Application.Current.MainWindow).Transform(new Point(0, 0));
            live15_grid.Tag = CreateSubView( gridPoint15.X, gridPoint15.Y, live15_grid.ActualWidth, live15_grid.ActualHeight, 0.0);

            Rect rect15;
            rect15.X = gridPoint15.X;
            rect15.Y = gridPoint15.Y;
            rect15.Width = live15_grid.ActualWidth;
            rect15.Height = live15_grid.ActualHeight;

            var rectangle15 = new Rectangle();
            rectangle15.Width = rect15.Width;
            rectangle15.Height = rect15.Height;

            var brush15 = new ImageBrush(_di)
            {
                Viewbox = rect15,
                ViewboxUnits = BrushMappingMode.Absolute,
            };

            rectangle15.Fill = brush15;
            canvas15.Children.Add(rectangle15);

            ///// view 16 ////
            Point gridPoint16 = live16_grid.TransformToAncestor(Application.Current.MainWindow).Transform(new Point(0, 0));
            live16_grid.Tag = CreateSubView( gridPoint16.X, gridPoint16.Y, live16_grid.ActualWidth, live16_grid.ActualHeight, 0.0);

            Rect rect16;
            rect16.X = gridPoint16.X;
            rect16.Y = gridPoint16.Y;
            rect16.Width = live16_grid.ActualWidth;
            rect16.Height = live16_grid.ActualHeight;

            var rectangle16 = new Rectangle();
            rectangle16.Width = rect16.Width;
            rectangle16.Height = rect16.Height;

            var brush16 = new ImageBrush(_di)
            {
                Viewbox = rect16,
                ViewboxUnits = BrushMappingMode.Absolute,
            };

            rectangle16.Fill = brush16;
            canvas16.Children.Add(rectangle16);

            ///// view 17 ////
            Point gridPoint17 = live17_grid.TransformToAncestor(Application.Current.MainWindow).Transform(new Point(0, 0));
            live17_grid.Tag = CreateSubView( gridPoint17.X, gridPoint17.Y, live17_grid.ActualWidth, live17_grid.ActualHeight, 0.0);

            Rect rect17;
            rect17.X = gridPoint17.X;
            rect17.Y = gridPoint17.Y;
            rect17.Width = live17_grid.ActualWidth;
            rect17.Height = live17_grid.ActualHeight;

            var rectangle17 = new Rectangle();
            rectangle17.Width = rect17.Width;
            rectangle17.Height = rect17.Height;

            var brush17 = new ImageBrush(_di)
            {
                Viewbox = rect17,
                ViewboxUnits = BrushMappingMode.Absolute,
            };

            rectangle17.Fill = brush17;
            canvas17.Children.Add(rectangle17);

            ///// view 18 ////
            Point gridPoint18 = live18_grid.TransformToAncestor(Application.Current.MainWindow).Transform(new Point(0, 0));
            live18_grid.Tag = CreateSubView( gridPoint18.X, gridPoint18.Y, live18_grid.ActualWidth, live18_grid.ActualHeight, 0.0);

            Rect rect18;
            rect18.X = gridPoint18.X;
            rect18.Y = gridPoint18.Y;
            rect18.Width = live18_grid.ActualWidth;
            rect18.Height = live18_grid.ActualHeight;

            var rectangle18 = new Rectangle();
            rectangle18.Width = rect18.Width;
            rectangle18.Height = rect18.Height;

            var brush18 = new ImageBrush(_di)
            {
                Viewbox = rect18,
                ViewboxUnits = BrushMappingMode.Absolute,
            };

            rectangle18.Fill = brush18;
            canvas18.Children.Add(rectangle18);

            ///// view 19 ////
            Point gridPoint19 = live19_grid.TransformToAncestor(Application.Current.MainWindow).Transform(new Point(0, 0));
            live19_grid.Tag = CreateSubView( gridPoint19.X, gridPoint19.Y, live19_grid.ActualWidth, live19_grid.ActualHeight, 0.0);

            Rect rect19;
            rect19.X = gridPoint19.X;
            rect19.Y = gridPoint19.Y;
            rect19.Width = live19_grid.ActualWidth;
            rect19.Height = live19_grid.ActualHeight;

            var rectangle19 = new Rectangle();
            rectangle19.Width = rect19.Width;
            rectangle19.Height = rect19.Height;

            var brush19 = new ImageBrush(_di)
            {
                Viewbox = rect19,
                ViewboxUnits = BrushMappingMode.Absolute,
            };

            rectangle19.Fill = brush19;
            canvas19.Children.Add(rectangle19);

            ///// view 20 ////
            Point gridPoint20 = live20_grid.TransformToAncestor(Application.Current.MainWindow).Transform(new Point(0, 0));
            live20_grid.Tag = CreateSubView( gridPoint20.X, gridPoint20.Y, live20_grid.ActualWidth, live20_grid.ActualHeight, 0.0);

            Rect rect20;
            rect20.X = gridPoint20.X;
            rect20.Y = gridPoint20.Y;
            rect20.Width = live20_grid.ActualWidth;
            rect20.Height = live20_grid.ActualHeight;

            var rectangle20 = new Rectangle();
            rectangle20.Width = rect20.Width;
            rectangle20.Height = rect20.Height;

            var brush20 = new ImageBrush(_di)
            {
                Viewbox = rect20,
                ViewboxUnits = BrushMappingMode.Absolute,
            };

            rectangle20.Fill = brush20;
            canvas20.Children.Add(rectangle20);

            ///// view 21 ////
            Point gridPoint21 = live21_grid.TransformToAncestor(Application.Current.MainWindow).Transform(new Point(0, 0));
            live21_grid.Tag = CreateSubView( gridPoint21.X, gridPoint21.Y, live21_grid.ActualWidth, live21_grid.ActualHeight, 0.0);

            Rect rect21;
            rect21.X = gridPoint21.X;
            rect21.Y = gridPoint21.Y;
            rect21.Width = live21_grid.ActualWidth;
            rect21.Height = live21_grid.ActualHeight;

            var rectangle21 = new Rectangle();
            rectangle21.Width = rect21.Width;
            rectangle21.Height = rect21.Height;

            var brush21 = new ImageBrush(_di)
            {
                Viewbox = rect20,
                ViewboxUnits = BrushMappingMode.Absolute,
            };

            rectangle21.Fill = brush21;
            canvas21.Children.Add(rectangle21);

            ///// view 22 ////
            Point gridPoint22 = live22_grid.TransformToAncestor(Application.Current.MainWindow).Transform(new Point(0, 0));
            live22_grid.Tag = CreateSubView( gridPoint22.X, gridPoint22.Y, live22_grid.ActualWidth, live22_grid.ActualHeight, 0.0);

            Rect rect22;
            rect22.X = gridPoint22.X;
            rect22.Y = gridPoint22.Y;
            rect22.Width = live22_grid.ActualWidth;
            rect22.Height = live22_grid.ActualHeight;

            var rectangle22 = new Rectangle();
            rectangle22.Width = rect22.Width;
            rectangle22.Height = rect22.Height;

            var brush22 = new ImageBrush(_di)
            {
                Viewbox = rect22,
                ViewboxUnits = BrushMappingMode.Absolute,
            };

            rectangle22.Fill = brush22;
            canvas22.Children.Add(rectangle22);

            ///// view 23 ////
            Point gridPoint23 = live23_grid.TransformToAncestor(Application.Current.MainWindow).Transform(new Point(0, 0));
            live23_grid.Tag = CreateSubView( gridPoint23.X, gridPoint23.Y, live23_grid.ActualWidth, live23_grid.ActualHeight, 0.0);

            Rect rect23;
            rect23.X = gridPoint23.X;
            rect23.Y = gridPoint23.Y;
            rect23.Width = live23_grid.ActualWidth;
            rect23.Height = live23_grid.ActualHeight;

            var rectangle23 = new Rectangle();
            rectangle23.Width = rect23.Width;
            rectangle23.Height = rect23.Height;

            var brush23 = new ImageBrush(_di)
            {
                Viewbox = rect23,
                ViewboxUnits = BrushMappingMode.Absolute,
            };

            rectangle23.Fill = brush23;
            canvas23.Children.Add(rectangle23);

            ///// view 24 ////
            Point gridPoint24 = live24_grid.TransformToAncestor(Application.Current.MainWindow).Transform(new Point(0, 0));
            live24_grid.Tag = CreateSubView( gridPoint24.X, gridPoint24.Y, live24_grid.ActualWidth, live24_grid.ActualHeight, 0.0);

            Rect rect24;
            rect24.X = gridPoint24.X;
            rect24.Y = gridPoint24.Y;
            rect24.Width = live24_grid.ActualWidth;
            rect24.Height = live24_grid.ActualHeight;

            var rectangle24 = new Rectangle();
            rectangle24.Width = rect24.Width;
            rectangle24.Height = rect24.Height;

            var brush24 = new ImageBrush(_di)
            {
                Viewbox = rect24,
                ViewboxUnits = BrushMappingMode.Absolute,
            };

            rectangle24.Fill = brush24;
            canvas24.Children.Add(rectangle24);

            ///// view 25 ////
            Point gridPoint25 = live25_grid.TransformToAncestor(Application.Current.MainWindow).Transform(new Point(0, 0));
            live25_grid.Tag = CreateSubView( gridPoint25.X, gridPoint25.Y, live25_grid.ActualWidth, live25_grid.ActualHeight, 0.0);

            Rect rect25;
            rect25.X = gridPoint25.X;
            rect25.Y = gridPoint25.Y;
            rect25.Width = live25_grid.ActualWidth;
            rect25.Height = live25_grid.ActualHeight;

            var rectangle25 = new Rectangle();
            rectangle25.Width = rect25.Width;
            rectangle25.Height = rect25.Height;

            var brush25 = new ImageBrush(_di)
            {
                Viewbox = rect25,
                ViewboxUnits = BrushMappingMode.Absolute,
            };

            rectangle25.Fill = brush25;
            canvas25.Children.Add(rectangle25);

            ///// view 26 ////
            Point gridPoint26 = live26_grid.TransformToAncestor(Application.Current.MainWindow).Transform(new Point(0, 0));
            live26_grid.Tag = CreateSubView( gridPoint26.X, gridPoint26.Y, live26_grid.ActualWidth, live26_grid.ActualHeight, 0.0);

            Rect rect26;
            rect26.X = gridPoint26.X;
            rect26.Y = gridPoint26.Y;
            rect26.Width = live26_grid.ActualWidth;
            rect26.Height = live26_grid.ActualHeight;

            var rectangle26 = new Rectangle();
            rectangle26.Width = rect26.Width;
            rectangle26.Height = rect26.Height;

            var brush26 = new ImageBrush(_di)
            {
                Viewbox = rect26,
                ViewboxUnits = BrushMappingMode.Absolute,
            };

            rectangle26.Fill = brush26;
            canvas26.Children.Add(rectangle26);

            ///// view 27 ////
            Point gridPoint27 = live27_grid.TransformToAncestor(Application.Current.MainWindow).Transform(new Point(0, 0));
            live27_grid.Tag = CreateSubView( gridPoint27.X, gridPoint27.Y, live27_grid.ActualWidth, live27_grid.ActualHeight, 0.0);

            Rect rect27;
            rect27.X = gridPoint27.X;
            rect27.Y = gridPoint27.Y;
            rect27.Width = live27_grid.ActualWidth;
            rect27.Height = live27_grid.ActualHeight;

            var rectangle27 = new Rectangle();
            rectangle27.Width = rect27.Width;
            rectangle27.Height = rect27.Height;

            var brush27 = new ImageBrush(_di)
            {
                Viewbox = rect27,
                ViewboxUnits = BrushMappingMode.Absolute,
            };

            rectangle27.Fill = brush27;
            canvas27.Children.Add(rectangle27);

            ///// view 28 ////
            Point gridPoint28 = live28_grid.TransformToAncestor(Application.Current.MainWindow).Transform(new Point(0, 0));
            live28_grid.Tag = CreateSubView( gridPoint28.X, gridPoint28.Y, live28_grid.ActualWidth, live28_grid.ActualHeight, 0.0);

            Rect rect28;
            rect28.X = gridPoint28.X;
            rect28.Y = gridPoint28.Y;
            rect28.Width = live28_grid.ActualWidth;
            rect28.Height = live28_grid.ActualHeight;

            var rectangle28 = new Rectangle();
            rectangle28.Width = rect28.Width;
            rectangle28.Height = rect28.Height;

            var brush28 = new ImageBrush(_di)
            {
                Viewbox = rect28,
                ViewboxUnits = BrushMappingMode.Absolute,
            };

            rectangle28.Fill = brush28;
            canvas28.Children.Add(rectangle28);

            ///// view 29 ////
            Point gridPoint29 = live29_grid.TransformToAncestor(Application.Current.MainWindow).Transform(new Point(0, 0));
            live29_grid.Tag = CreateSubView( gridPoint29.X, gridPoint29.Y, live29_grid.ActualWidth, live29_grid.ActualHeight, 0.0);

            Rect rect29;
            rect29.X = gridPoint29.X;
            rect29.Y = gridPoint29.Y;
            rect29.Width = live29_grid.ActualWidth;
            rect29.Height = live29_grid.ActualHeight;

            var rectangle29 = new Rectangle();
            rectangle29.Width = rect29.Width;
            rectangle29.Height = rect29.Height;

            var brush29 = new ImageBrush(_di)
            {
                Viewbox = rect29,
                ViewboxUnits = BrushMappingMode.Absolute,
            };

            rectangle29.Fill = brush29;
            canvas29.Children.Add(rectangle29);

            ///// view 30 ////
            Point gridPoint30 = live30_grid.TransformToAncestor(Application.Current.MainWindow).Transform(new Point(0, 0));
            live30_grid.Tag = CreateSubView( gridPoint30.X, gridPoint30.Y, live30_grid.ActualWidth, live30_grid.ActualHeight, 0.0);

            Rect rect30;
            rect30.X = gridPoint30.X;
            rect30.Y = gridPoint30.Y;
            rect30.Width = live30_grid.ActualWidth;
            rect30.Height = live30_grid.ActualHeight;

            var rectangle30 = new Rectangle();
            rectangle30.Width = rect30.Width;
            rectangle30.Height = rect30.Height;

            var brush30 = new ImageBrush(_di)
            {
                Viewbox = rect30,
                ViewboxUnits = BrushMappingMode.Absolute,
            };

            rectangle30.Fill = brush30;
            canvas30.Children.Add(rectangle30);

            ///// view 31 ////
            Point gridPoint31 = live31_grid.TransformToAncestor(Application.Current.MainWindow).Transform(new Point(0, 0));
            live31_grid.Tag = CreateSubView( gridPoint31.X, gridPoint31.Y, live31_grid.ActualWidth, live31_grid.ActualHeight, 0.0);

            Rect rect31;
            rect31.X = gridPoint31.X;
            rect31.Y = gridPoint31.Y;
            rect31.Width = live31_grid.ActualWidth;
            rect31.Height = live31_grid.ActualHeight;

            var rectangle31 = new Rectangle();
            rectangle31.Width = rect31.Width;
            rectangle31.Height = rect31.Height;

            var brush31 = new ImageBrush(_di)
            {
                Viewbox = rect31,
                ViewboxUnits = BrushMappingMode.Absolute,
            };

            rectangle31.Fill = brush31;
            canvas31.Children.Add(rectangle31);

            ///// view 32 ////
            Point gridPoint32 = live32_grid.TransformToAncestor(Application.Current.MainWindow).Transform(new Point(0, 0));
            live32_grid.Tag = CreateSubView(gridPoint32.X, gridPoint32.Y, live32_grid.ActualWidth, live32_grid.ActualHeight, 0.0);

            Rect rect32;
            rect32.X = gridPoint32.X;
            rect32.Y = gridPoint32.Y;
            rect32.Width = live32_grid.ActualWidth;
            rect32.Height = live32_grid.ActualHeight;

            var rectangle32 = new Rectangle();
            rectangle32.Width = rect32.Width;
            rectangle32.Height = rect32.Height;

            var brush32 = new ImageBrush(_di)
            {
                Viewbox = rect32,
                ViewboxUnits = BrushMappingMode.Absolute,
            };

            rectangle32.Fill = brush32;
            canvas32.Children.Add(rectangle32);

        }

        void OnPlay1(object sender, RoutedEventArgs e)
        {
            AddViewSource((int)live1_grid.Tag, Url1.Text, "admin", "jeonju0683", false);
        }

        void OnStop1(object sender, RoutedEventArgs e)
        {
            RemoveViewSource((int)live1_grid.Tag);
            RemoveSubView((int)live1_grid.Tag);
            live1_grid.Children.Clear();
        }

        void OnPlay2(object sender, RoutedEventArgs e)
        {
            AddViewSource((int)live2_grid.Tag, Url2.Text, "admin", "jeonju0683", false);
        }

        void OnStop2(object sender, RoutedEventArgs e)
        {
            RemoveViewSource((int)live2_grid.Tag);
            RemoveSubView((int)live1_grid.Tag);
            live2_grid.Children.Clear();
        }

        void OnPlay3(object sender, RoutedEventArgs e)
        {
            AddViewSource((int)live3_grid.Tag, Url3.Text, "admin", "jeonju0690", false);
        }

        void OnStop3(object sender, RoutedEventArgs e)
        {
            RemoveViewSource((int)live3_grid.Tag);
            RemoveSubView((int)live3_grid.Tag);
            live3_grid.Children.Clear();
        }

        void OnPlay4(object sender, RoutedEventArgs e)
        {
            AddViewSource((int)live4_grid.Tag, Url4.Text, "admin", "jeonju0690", false);
        }

        void OnStop4(object sender, RoutedEventArgs e)
        {
            RemoveViewSource((int)live4_grid.Tag);
            RemoveSubView((int)live4_grid.Tag);
            live4_grid.Children.Clear();
        }

        void OnPlay5(object sender, RoutedEventArgs e)
        {
            AddViewSource((int)live5_grid.Tag, Url5.Text, "admin", "123456", false);
        }

        void OnStop5(object sender, RoutedEventArgs e)
        {
            RemoveViewSource((int)live5_grid.Tag);
            RemoveSubView((int)live5_grid.Tag);
            live5_grid.Children.Clear();
        }
        void OnPlay6(object sender, RoutedEventArgs e)
        {
            AddViewSource((int)live6_grid.Tag, Url6.Text, "", "", false);
        }

        void OnStop6(object sender, RoutedEventArgs e)
        {
            RemoveViewSource((int)live6_grid.Tag);
            RemoveSubView((int)live6_grid.Tag);
            live6_grid.Children.Clear();
        }
        void OnPlay7(object sender, RoutedEventArgs e)
        {
            AddViewSource((int)live7_grid.Tag, Url7.Text, "", "", false);
        }

        void OnStop7(object sender, RoutedEventArgs e)
        {
            RemoveViewSource((int)live7_grid.Tag);
            RemoveSubView((int)live7_grid.Tag);
            live7_grid.Children.Clear();
        }
        void OnPlay8(object sender, RoutedEventArgs e)
        {
            AddViewSource((int)live8_grid.Tag, Url8.Text, "", "", false);
        }

        void OnStop8(object sender, RoutedEventArgs e)
        {
            RemoveViewSource((int)live8_grid.Tag);
            RemoveSubView((int)live8_grid.Tag);
            live8_grid.Children.Clear();
        }
        void OnPlay9(object sender, RoutedEventArgs e)
        {
            AddViewSource((int)live9_grid.Tag, Url9.Text, "", "", false);
        }

        void OnStop9(object sender, RoutedEventArgs e)
        {
            RemoveViewSource((int)live9_grid.Tag);
            RemoveSubView((int)live9_grid.Tag);
            live9_grid.Children.Clear();
        }
        void OnPlay10(object sender, RoutedEventArgs e)
        {
            AddViewSource((int)live10_grid.Tag, Url10.Text, "", "", false);
        }

        void OnStop10(object sender, RoutedEventArgs e)
        {
            RemoveViewSource((int)live10_grid.Tag);
            RemoveSubView((int)live10_grid.Tag);
            live10_grid.Children.Clear();
        }
        void OnPlay11(object sender, RoutedEventArgs e)
        {
            AddViewSource((int)live11_grid.Tag, Url11.Text, "", "", false);
        }

        void OnStop11(object sender, RoutedEventArgs e)
        {
            RemoveViewSource((int)live11_grid.Tag);
            RemoveSubView((int)live11_grid.Tag);
            live11_grid.Children.Clear();
        }
        void OnPlay12(object sender, RoutedEventArgs e)
        {
            AddViewSource((int)live12_grid.Tag, Url12.Text, "", "", false);
        }

        void OnStop12(object sender, RoutedEventArgs e)
        {
            RemoveViewSource((int)live12_grid.Tag);
            RemoveSubView((int)live12_grid.Tag);
            live12_grid.Children.Clear();
        }
        void OnPlay13(object sender, RoutedEventArgs e)
        {
            AddViewSource((int)live13_grid.Tag, Url13.Text, "", "", false);
        }

        void OnStop13(object sender, RoutedEventArgs e)
        {
            RemoveViewSource((int)live13_grid.Tag);
            RemoveSubView((int)live13_grid.Tag);
            live13_grid.Children.Clear();
        }
        void OnPlay14(object sender, RoutedEventArgs e)
        {
            AddViewSource((int)live14_grid.Tag, Url14.Text, "", "", false);
        }

        void OnStop14(object sender, RoutedEventArgs e)
        {
            RemoveViewSource((int)live14_grid.Tag);
            RemoveSubView((int)live14_grid.Tag);
            live14_grid.Children.Clear();
        }
        void OnPlay15(object sender, RoutedEventArgs e)
        {
            AddViewSource((int)live15_grid.Tag, Url15.Text, "", "", false);
        }

        void OnStop15(object sender, RoutedEventArgs e)
        {
            RemoveViewSource((int)live15_grid.Tag);
            RemoveSubView((int)live15_grid.Tag);
            live15_grid.Children.Clear();
        }
        void OnPlay16(object sender, RoutedEventArgs e)
        {
            AddViewSource((int)live16_grid.Tag, Url16.Text, "", "", false);
        }

        void OnStop16(object sender, RoutedEventArgs e)
        {
            RemoveViewSource((int)live16_grid.Tag);
            RemoveSubView((int)live16_grid.Tag);
            live16_grid.Children.Clear();
        }
        void OnPlay17(object sender, RoutedEventArgs e)
        {
            AddViewSource((int)live17_grid.Tag, Url17.Text, "", "", false);
        }

        void OnStop17(object sender, RoutedEventArgs e)
        {
            RemoveViewSource((int)live17_grid.Tag);
            RemoveSubView((int)live17_grid.Tag);
            live17_grid.Children.Clear();
        }
        void OnPlay18(object sender, RoutedEventArgs e)
        {
            AddViewSource((int)live18_grid.Tag, Url18.Text, "", "", false);
        }

        void OnStop18(object sender, RoutedEventArgs e)
        {
            RemoveViewSource((int)live18_grid.Tag);
            RemoveSubView((int)live18_grid.Tag);
            live18_grid.Children.Clear();
        }
        void OnPlay19(object sender, RoutedEventArgs e)
        {
            AddViewSource((int)live19_grid.Tag, Url19.Text, "", "", false);
        }

        void OnStop19(object sender, RoutedEventArgs e)
        {
            RemoveViewSource((int)live19_grid.Tag);
            RemoveSubView((int)live19_grid.Tag);
            live19_grid.Children.Clear();
        }
        void OnPlay20(object sender, RoutedEventArgs e)
        {
            AddViewSource((int)live20_grid.Tag, Url20.Text, "", "", false);
        }

        void OnStop20(object sender, RoutedEventArgs e)
        {
            RemoveViewSource((int)live20_grid.Tag);
            RemoveSubView((int)live20_grid.Tag);
            live20_grid.Children.Clear();
        }
        void OnPlay21(object sender, RoutedEventArgs e)
        {
            AddViewSource((int)live21_grid.Tag, Url21.Text, "", "", false);
        }

        void OnStop21(object sender, RoutedEventArgs e)
        {
            RemoveViewSource((int)live21_grid.Tag);
            RemoveSubView((int)live21_grid.Tag);
            live21_grid.Children.Clear();
        }
        void OnPlay22(object sender, RoutedEventArgs e)
        {
            AddViewSource((int)live22_grid.Tag, Url22.Text, "", "", false);
        }

        void OnStop22(object sender, RoutedEventArgs e)
        {
            RemoveViewSource((int)live22_grid.Tag);
            RemoveSubView((int)live22_grid.Tag);
            live22_grid.Children.Clear();
        }
        void OnPlay23(object sender, RoutedEventArgs e)
        {
            AddViewSource((int)live23_grid.Tag, Url23.Text, "", "", false);
        }

        void OnStop23(object sender, RoutedEventArgs e)
        {
            RemoveViewSource((int)live23_grid.Tag);
            RemoveSubView((int)live23_grid.Tag);
            live23_grid.Children.Clear();
        }
        void OnPlay24(object sender, RoutedEventArgs e)
        {
            AddViewSource((int)live24_grid.Tag, Url24.Text, "", "", false);
        }

        void OnStop24(object sender, RoutedEventArgs e)
        {
            RemoveViewSource((int)live24_grid.Tag);
            RemoveSubView((int)live24_grid.Tag);
            live24_grid.Children.Clear();
        }
        void OnPlay25(object sender, RoutedEventArgs e)
        {
            AddViewSource((int)live25_grid.Tag, Url25.Text, "", "", false);
        }

        void OnStop25(object sender, RoutedEventArgs e)
        {
            RemoveViewSource((int)live25_grid.Tag);
            RemoveSubView((int)live25_grid.Tag);
            live25_grid.Children.Clear();
        }
        void OnPlay26(object sender, RoutedEventArgs e)
        {
            AddViewSource((int)live26_grid.Tag, Url26.Text, "", "", false);
        }

        void OnStop26(object sender, RoutedEventArgs e)
        {
            RemoveViewSource((int)live26_grid.Tag);
            RemoveSubView((int)live26_grid.Tag);
            live26_grid.Children.Clear();
        }
        void OnPlay27(object sender, RoutedEventArgs e)
        {
            AddViewSource((int)live27_grid.Tag, Url27.Text, "", "", false);
        }

        void OnStop27(object sender, RoutedEventArgs e)
        {
            RemoveViewSource((int)live27_grid.Tag);
            RemoveSubView((int)live27_grid.Tag);
            live27_grid.Children.Clear();
        }
        void OnPlay28(object sender, RoutedEventArgs e)
        {
            AddViewSource((int)live28_grid.Tag, Url28.Text, "", "", false);
        }

        void OnStop28(object sender, RoutedEventArgs e)
        {
            RemoveViewSource((int)live28_grid.Tag);
            RemoveSubView((int)live28_grid.Tag);
            live28_grid.Children.Clear();
        }
        void OnPlay29(object sender, RoutedEventArgs e)
        {
            AddViewSource((int)live29_grid.Tag, Url29.Text, "", "", false);
        }

        void OnStop29(object sender, RoutedEventArgs e)
        {
            RemoveViewSource((int)live29_grid.Tag);
            RemoveSubView((int)live29_grid.Tag);
            live29_grid.Children.Clear();
        }
        void OnPlay30(object sender, RoutedEventArgs e)
        {
            AddViewSource((int)live30_grid.Tag, Url30.Text, "", "", false);
        }

        void OnStop30(object sender, RoutedEventArgs e)
        {
            RemoveViewSource((int)live30_grid.Tag);
            RemoveSubView((int)live30_grid.Tag);
            live30_grid.Children.Clear();
        }
        void OnPlay31(object sender, RoutedEventArgs e)
        {
            AddViewSource((int)live31_grid.Tag, Url31.Text, "", "", false);
        }

        void OnStop31(object sender, RoutedEventArgs e)
        {
            RemoveViewSource((int)live31_grid.Tag);
            RemoveSubView((int)live31_grid.Tag);
            live31_grid.Children.Clear();
        }
        void OnPlay32(object sender, RoutedEventArgs e)
        {
            AddViewSource((int)live32_grid.Tag, Url32.Text, "", "", false);
        }

        void OnStop32(object sender, RoutedEventArgs e)
        {
            RemoveViewSource((int)live32_grid.Tag);
            RemoveSubView((int)live32_grid.Tag);
            live32_grid.Children.Clear();
        }
        void OnPlay33(object sender, RoutedEventArgs e)
        {
            AddViewSource((int)live33_grid.Tag, Url33.Text, "", "", false);
        }

        void OnStop33(object sender, RoutedEventArgs e)
        {
            RemoveViewSource((int)live33_grid.Tag);
            RemoveSubView((int)live33_grid.Tag);
            live33_grid.Children.Clear();
        }
        void OnPlay34(object sender, RoutedEventArgs e)
        {
            AddViewSource((int)live34_grid.Tag, Url34.Text, "", "", false);
        }

        void OnStop34(object sender, RoutedEventArgs e)
        {
            RemoveViewSource((int)live34_grid.Tag);
            RemoveSubView((int)live34_grid.Tag);
            live34_grid.Children.Clear();
        }
        void OnPlay35(object sender, RoutedEventArgs e)
        {
            AddViewSource((int)live35_grid.Tag, Url35.Text, "", "", false);
        }

        void OnStop35(object sender, RoutedEventArgs e)
        {
            RemoveViewSource((int)live35_grid.Tag);
            RemoveSubView((int)live35_grid.Tag);
            live35_grid.Children.Clear();
        }
        void OnPlay36(object sender, RoutedEventArgs e)
        {
            AddViewSource((int)live36_grid.Tag, Url36.Text, "", "", false);
        }

        void OnStop36(object sender, RoutedEventArgs e)
        {
            RemoveViewSource((int)live36_grid.Tag);
            RemoveSubView((int)live36_grid.Tag);
            live36_grid.Children.Clear();
        }
        private void BeginRenderingScene()
        {
            if (_di.IsFrontBufferAvailable)
            {
                GetBackBuffer(out _scene);

                _di.Lock();
                _di.SetBackBuffer(D3DResourceType.IDirect3DSurface9, _scene, true);
                _di.Unlock();

                CompositionTarget.Rendering += OnRendering;
            }
        }
        private void UpdateScene()
        {
            GetBackBuffer(out _scene);

            if (_scene != IntPtr.Zero)
            {
                _di.Lock();

                _di.SetBackBuffer(D3DResourceType.IDirect3DSurface9, _scene, true);
                Display();
                _di.AddDirtyRect(new Int32Rect(0, 0, _di.PixelWidth, _di.PixelHeight));

                _di.Unlock();
            }
        }

        private void StopRenderingScene()
        {
            CompositionTarget.Rendering -= OnRendering;

            _scene = IntPtr.Zero;
        }

        private void OnRendering(object sender, EventArgs e)
        {
            RenderingEventArgs args = (RenderingEventArgs)e;

            //if (_di.IsFrontBufferAvailable && args.RenderingTime != _lastRenderTime)
            if (args.RenderingTime != _lastRenderTime)
            {
                UpdateScene();
                _lastRenderTime = args.RenderingTime;
            }
        }

        private void OnIsFrontBufferAvailableChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (_di.IsFrontBufferAvailable)
            {
                BeginRenderingScene();
            }
            else
            {
                //StopRenderingScene();
            }
        }

        [DllImport("xmedia.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        static extern void LoadXmediaPlayer(string logfilePath);

        [DllImport("xmedia.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        static extern void CreateViewControl(IntPtr wnd);

        [DllImport("xmedia.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        static extern int InitializeViewControl();

        [DllImport("xmedia.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        static extern void ReleaseViewControl();

        [DllImport("xmedia.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        static extern int CreateSubView(double x, double y, double w, double h, double border);

        [DllImport("xmedia.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        static extern void RemoveSubView(int boxid);

        [DllImport("xmedia.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        static extern void AddViewSource(int boxid, string url, string user, string password, bool useTCP);

        [DllImport("xmedia.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        static extern void RemoveViewSource(int boxid);

        [DllImport("xmedia.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        static extern void GetBackBuffer(out IntPtr backbuffer);

        [DllImport("xmedia.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        static extern void Display();

        [DllImport("xmedia.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        static extern void Present(IntPtr wnd);
    }
}
